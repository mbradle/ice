////////////////////////////////////////////////////////////////////////////////
//  
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header for defines for utilities in multi-zone chemical
//!        evolution code.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef ICE_UTILITIES_DEF_H
#define ICE_UTILITIES_DEF_H

//##############################################################################
// String defines.
//##############################################################################

#define S_N_X_ZONES             "n_x_zones"
#define S_N_Y_ZONES             "n_y_zones"
#define S_N_Z_ZONES             "n_z_zones"
#define S_SEED                  "seed"
#define S_TOTAL_MASS            "total_mass"
#define S_TOTAL                 "total"
#define S_DTMAX                 "dtmax"
#define S_SMALL_ABUND           "small_abund"
#define S_SMALL_RATES           "small_rates"
#define S_STARS_HEAP            "stars heap"
#define S_MASS_MAP              "mass map"

#define S_FORMATION_TIME  	"formation time"
#define S_END_TIME  	        "end time"
#define S_ID                    "id"
#define S_MASS      		"mass"
#define S_INITIAL_MASS      	"initial mass"
#define S_INITIAL_METALLICITY   "initial metallicity"
#define S_MY_X          	"x"
#define S_MY_Y          	"y"
#define S_MY_Z          	"z"
#define S_MY_REAL_X          	"real x"
#define S_MY_REAL_Y          	"real y"
#define S_MY_REAL_Z          	"real z"

#define S_METALLICITY           "metallicity"
#define S_NUCNET                "nucnet"
#define S_NUC_XPATH             "nuc_xpath"
#define S_REAC_XPATH            "reac_xpath"
#define S_ZONE_XPATH            "zone_xpath"
#define S_STARS_NUCNET          "stars nucnet"
#define S_ZONE_HISTORY_NUCNET   "zone history nucnet"
#define S_CLOUD_MIX             "cloud_mix"
#define S_GRAPH_FILE            "graph_file"
#define S_GRAPH_STEP            "graph_step"
#define S_HDF5                  "hdf5_file"
#define S_HISTORY_STEPS         "history_steps"
#define S_HISTORY_DTIME         "history dt"
#define S_ITMAX                 "it_max"
#define S_REL_TOL               "relative_tolerance"
#define S_DEBUG                 "debug"
#define S_NUMBER                "number"
#define S_SCHMIDT_EXPONENT      "schmidt_exponent"
#define S_STAR                  "star"
#define S_ACTIVE_STAR           "active star"
#define S_INACTIVE_STAR         "inactive star"
#define S_WHITE_DWARF           "white dwarf"
#define S_NEUTRON_STAR          "neutron star"
#define S_BLACK_HOLE            "black hole"
#define S_GAS                   "gas"
#define S_HALO                  "halo"
#define S_STAR_FORM             "star_form"
#define S_STAR_FILE_ID          "star_file_id"
#define S_STARS_HDF5            "stars_hdf5_file"
#define S_STEP                  "step"
#define S_ZONE_ID_MAP           "zone id map"

//##############################################################################
// Other defines.
//##############################################################################

#define D_YEAR                  3.15e7
#define D_MIN_ABUND     	0.

#endif  // ICE_UTILITIES_DEF_H
