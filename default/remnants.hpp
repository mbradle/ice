////////////////////////////////////////////////////////////////////////////////
//  
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header for standard remnants code.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <algorithm>
#include <functional>
#include <iostream>
#include <set>
#include <map>

#include "nnt/iter.h"

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef REMNANTS_HPP
#define REMNANTS_HPP

//##############################################################################
// Strings.
//##############################################################################

#define S_IA_FRACTION               "Ia_fraction"
#define S_IA_TIME                   "Ia_time"
#define S_NS_MERGER_TIME            "ns_merger_time"
#define S_NS_MERGER_FRACTION        "ns_merger_fraction"
#define S_IA_FILE                   "Ia_file"
#define S_NS_MERGER_FILE            "ns_merger_file"

//##############################################################################
// get_remnants_descriptions().
//##############################################################################

void
get_remnants_descriptions( po::options_description& remnants )
{

  try
  {

    remnants.add_options()

    // Option for Ia fraction
    ( S_IA_FRACTION, po::value<double>()->default_value( 0.05, "0.05" ),
      "Fraction of star/white dwarf systems that will become Ias" )
    
    // Option for Ia timescale
    ( S_IA_TIME, po::value<double>()->default_value( 1.e8, "1.e8" ),
      "Timescale (years) for Ia explosion on star/white dwarf system" )
    
    // Option for Ia timescale
    ( S_IA_FILE, po::value<std::string>(),
      "Name of text file to record Ia events (default: not set)" )
    
    // Option for NS-NS merger timescale
    (
      S_NS_MERGER_FRACTION,
      po::value<double>()->default_value( 0.01, "0.01" ),
      "Fraction of neutron star/neutron star systems that will merge"
    )
    
    (
      S_NS_MERGER_TIME,
      po::value<double>()->default_value( 2.e8, "2.e8" ),
      "Timescale (years) for neutron star/neutron star merger"
    )
    
    // Option for Ia timescale
    ( S_NS_MERGER_FILE, po::value<std::string>(),
      "Name of text file to record ns-ns merger events (default: not set)" );
    
// Add checks on input.
  
  }
  catch( std::exception& e )
  {
    std::cerr << "Error: " << e.what() << "\n";
    exit( EXIT_FAILURE );
  }
  catch(...)
  {
    std::cerr << "Exception of unknown type!\n";
    exit( EXIT_FAILURE );
  }
}

//##############################################################################
// set_remnants_data().
//##############################################################################

void
set_remnants_data(
  ice_data_t& ice_data,
  po::variables_map& vmap
)
{

  if( vmap.count(S_IA_FRACTION) )
  {
    ice_data[S_IA_FRACTION] = vmap[S_IA_FRACTION].as<double>();
  }

  if( vmap.count(S_IA_TIME) )
  {
    ice_data[S_IA_TIME] = vmap[S_IA_TIME].as<double>() * D_YEAR;
  }

  if( vmap.count(S_NS_MERGER_TIME) )
  {
    ice_data[S_NS_MERGER_TIME] =
      vmap[S_NS_MERGER_TIME].as<double>() * D_YEAR;
  }

  if( vmap.count(S_NS_MERGER_FRACTION) )
  {
    ice_data[S_NS_MERGER_FRACTION] = vmap[S_NS_MERGER_FRACTION].as<double>();
  }

  if( vmap.count(S_IA_FILE) )
  {
    ice_data[S_IA_FILE] = vmap[S_IA_FILE].as<std::string>();

    std::ofstream my_file;
    my_file.open(
      boost::any_cast<std::string>( ice_data[S_IA_FILE] ).c_str()
    );
    my_file.close();
  }

  if( vmap.count(S_NS_MERGER_FILE) )
  {
    ice_data[S_NS_MERGER_FILE] = vmap[S_NS_MERGER_FILE].as<std::string>();

    std::ofstream my_file;
    my_file.open(
      boost::any_cast<std::string>( ice_data[S_NS_MERGER_FILE] ).c_str()
    );
    my_file.close();
  }

}

//##############################################################################
// get_new_status().
//##############################################################################

std::string
get_new_status( Star& star, double remnant_mass )
{
  std::string status;
  if( star.getMass() < 10 )
  {
    status = S_WHITE_DWARF;
  }
  else
  {
    if( remnant_mass < 2 )
      status = S_NEUTRON_STAR;
    else
      status = S_BLACK_HOLE;
  }
  return status;
}

//##############################################################################
// update_star_in_single_star_system().
//##############################################################################

void
update_star_in_single_star_system(
  StarSystem& star_system,
  Star& star,
  double remnant_mass
)
{
  star.updateStatus( get_new_status( star, remnant_mass ) );
  star.updateEndTime( GSL_POSINF );
  star.updateCurrentMass( remnant_mass ); 

  star_system.popTopStar();
  star_system.addStar( star );
}

//##############################################################################
// update_star_in_multi_star_system().
//##############################################################################

void
update_star_in_multi_star_system(
  ice_data_t& ice_data,
  StarSystem& star_system,
  std::vector<Star>& v,
  double remnant_mass
)
{

  boost::random::random_device dev;
  boost::mt19937 my_gen( dev );

  boost::random::uniform_01<> uni_dist;

  boost::variate_generator<
    boost::mt19937&, boost::random::uniform_01<>
  > uni( my_gen, uni_dist );

  std::string status = get_new_status( v[0], remnant_mass );
  if( status == S_WHITE_DWARF )
  {
    if( v[1].getStatus() == S_STAR )
    {  
      v[0].updateStatus( status );
      if( uni() < boost::any_cast<double>( ice_data[S_IA_FRACTION] ) )
      {
        v[0].updateEndTime(
          boost::any_cast<double>( ice_data[S_IA_TIME] )
        );
      }
      else
      {
        v[0].updateEndTime( GSL_POSINF );
      }
      v[0].updateCurrentMass( remnant_mass ); 

      star_system.popTopStar();
      star_system.addStar( v[0] );
    }
    else
    {
      v[0].updateStatus( status );
      v[0].updateCurrentMass( remnant_mass );
      BOOST_FOREACH( Star s, v )
      {
        star_system.popTopStar();
      }
      BOOST_FOREACH( Star s, v )
      {
        s.updateEndTime( GSL_POSINF );
        star_system.addStar( s );
      }
    }
  }  
  else
  {
    if( v[1].getStatus() == S_STAR )
    {  
      v[0].updateStatus( status );
      v[0].updateEndTime( GSL_POSINF );
      v[0].updateCurrentMass( remnant_mass ); 

      star_system.popTopStar();
      star_system.addStar( v[0] );
    }
    else if( status == S_NEUTRON_STAR && v[1].getStatus() == S_NEUTRON_STAR )
    {
      double d_merge_time = GSL_POSINF;
      v[0].updateStatus( status );
      v[0].updateCurrentMass( remnant_mass );
      if( uni() < boost::any_cast<double>( ice_data[S_NS_MERGER_FRACTION] ) )
      {
        d_merge_time = 
          boost::any_cast<double>( ice_data[S_NS_MERGER_TIME] );
      }
      for( size_t i = 0; i < 2; i++ )
      {
        star_system.popTopStar();
      }
      for( size_t i = 0; i < 2; i++ )
      {
        v[i].updateEndTime( d_merge_time );
        star_system.addStar( v[i] );
      }
    }
    else
    {
      v[0].updateStatus( status );
      v[0].updateCurrentMass( remnant_mass );
      BOOST_FOREACH( Star s, v )
      {
        star_system.popTopStar();
      }
      BOOST_FOREACH( Star s, v )
      {
        s.updateEndTime( GSL_POSINF );
        star_system.addStar( s );
      }
    }
  }
}

//##############################################################################
// update_system_for_typeIa_supernova().
//##############################################################################

void
update_system_for_typeIa_supernova(
  ice_data_t& ice_data,
  StarSystem& star_system
)
{

  if( ice_data.find( S_IA_FILE ) != ice_data.end() )
  {

    Star s = star_system.getTopStar();

    std::ofstream my_file;

    my_file.open(
      boost::any_cast<std::string>( ice_data[S_IA_FILE] ).c_str(),
      std::ios::app
    );

    my_file << boost::any_cast<double>( ice_data[nnt::s_TIME] ) << "  " <<
      s.getMass() << "  " << s.getX() << "  " << s.getY() << "  " <<
      s.getZ() << std::endl;

    my_file.close();

  }

  star_system.popTopStar();

}

//##############################################################################
// update_system_neutron_star_merger().
//##############################################################################

void
update_system_for_neutron_star_merger(
  ice_data_t& ice_data,
  StarSystem& star_system,
  std::vector<Star>& v,
  double remnant_mass
)
{

  if( ice_data.find( S_NS_MERGER_FILE ) != ice_data.end() )
  {

    std::ofstream my_file;

    my_file.open(
      boost::any_cast<std::string>( ice_data[S_NS_MERGER_FILE] ).c_str(),
      std::ios::app
    );

    my_file << boost::any_cast<double>( ice_data[nnt::s_TIME] ) << "  " <<
      v[0].getOriginalMass() << "  " << v[1].getOriginalMass() << "  " <<
      v[0].getMass() << "  " << v[1].getMass() << "  " <<
      v[0].getX() << "  " <<
      v[0].getY() << "  " << v[0].getZ() << std::endl;

    my_file.close();

  }

  star_system.popTopStar();

  Star star = star_system.getTopStar();
  star_system.popTopStar();
  star.updateStatus( S_BLACK_HOLE );
  star.updateCurrentMass( remnant_mass );
  star.updateEndTime( GSL_POSINF );

  star_system.addStar( star );
  
}

//##############################################################################
// update_stellar_system().
//##############################################################################

void
update_stellar_system(
  ice_data_t& ice_data,
  StarSystem& star_system,
  double remnant_mass
)
{

  std::vector<Star> v = star_system.getStarVector();

  if( v.size() == 1 )
  {
    update_star_in_single_star_system( star_system, v[0], remnant_mass );
  }
  else
  {
    if( v[0].getStatus() == S_STAR )
    {
      update_star_in_multi_star_system(
        ice_data, star_system, v, remnant_mass
      );
    }
    else if( v[0].getStatus() == S_WHITE_DWARF )
    {
      update_system_for_typeIa_supernova( ice_data, star_system );
    }
    else if( v[0].getStatus() == S_NEUTRON_STAR )
    {
      update_system_for_neutron_star_merger(
        ice_data, star_system, v, remnant_mass
      );
    }
  }

}

//##############################################################################
// update_stellar_system_heap().
//##############################################################################

void
update_stellar_system_heap(
  ice_data_t& ice_data,
  heap_t& heap,
  StarSystem& star_system
)
{

  if(
    star_system.getTopStar().getEndTime() <
    boost::any_cast<double>( ice_data[nnt::s_TEND] )
  )
  {
    heap.push( star_system );
  }

}

#endif  // REMNANTS_HPP
