//////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header code to compute IMF deviates.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef STAR_POP_HPP
#define STAR_POP_HPP

//##############################################################################
// Includes.
//##############################################################################

#include <boost/numeric/odeint.hpp>

//##############################################################################
// Defines.
//##############################################################################

#define S_DEV_FILE              "dev_file"
#define S_IMF_ML                "imf_ml"
#define S_IMF_MU                "imf_mu"
#define S_LOW_MASS_STAR_MASS    "low-mass star mass"
#define S_MASS_FILE             "mass_file"
#define S_POP_HIST              "pop_hist"
#define S_POP_FILE              "pop_file"
#define S_POP_PTS               "pop_pts"

typedef boost::tuple<double, double, size_t, double> pop_tup_t;

//##############################################################################
// imf().  Kroupa IMF.
//##############################################################################

class imf
{
  public:

    imf( ice_data_t& _ice_data ) : ice_data( _ice_data )
    {
      ml = boost::any_cast<double>( ice_data[S_IMF_ML] );
      m1 = 0.08;
      m2 = 0.5;
      mu = boost::any_cast<double>( ice_data[S_IMF_MU] );
      if( ml > m1 )
      {
        std::cerr <<
          "IMF lower mass limit greater than m1 = 0.08." << std::endl;
        exit( EXIT_FAILURE );
      }
      if( mu < m2 )
      {
        std::cerr <<
          "IMF upper mass limit less than m2 = 0.5." << std::endl;
        exit( EXIT_FAILURE );
      }
      alpha1 = 0.3;
      alpha2 = 1.3;
      alpha3 = 2.3;
      C2 = pow( m1, alpha2 - alpha1 );
      C3 = pow( m2, alpha3 - alpha2 ) * C2;
    }

    double getLowerMass() const { return ml; }
    double getUpperMass() const { return mu; }

    double computeValue( const double m )
    {

      if( m >= ml && m < m1 )
        return pow( m, -alpha1 );
      else if( m >= m1 && m < m2 )
        return C2 * pow( m, -alpha2 );
      else if( m >= m2 && m <= 1.01 * mu )
        return C3 * pow( m, -alpha3 );
      else
      {
        std::cerr << m << " is an invalid mass for IMF." << std::endl;
        exit( EXIT_FAILURE );
      }

    }

  private:
    double m1, m2, ml, mu;
    double alpha1, alpha2, alpha3;
    double C2, C3;
    ice_data_t& ice_data;

};

//##############################################################################
// rhs().
//##############################################################################

struct rhs
{
  imf& my_imf;
  rhs( imf& _t ) : my_imf( _t ) {}
  void operator()( const double y , double &dydm , const double m )
  {
    dydm = my_imf.computeValue( m );
  }
};

//##############################################################################
// deviate_observer().
//##############################################################################

struct deviate_observer
{
  std::vector<std::pair<double, double> >& v;
  deviate_observer( std::vector<std::pair<double, double> >& _v ) : v( _v ) {}
  void operator()( const double &y, const double m )
  {
    v.push_back( std::make_pair( y, m ) );
  }
};

//##############################################################################
// imf_deviate().
//##############################################################################

class imf_deviate
{

  public:

    imf_deviate( ice_data_t& _ice_data ) : ice_data( _ice_data )
    {
      imf my_imf( ice_data );
      rhs my_rhs( my_imf );
      deviate_observer my_dev( v );
      typedef boost::numeric::odeint::runge_kutta_dopri5<double> stepper_type;
      std::vector<double> masses;

      size_t n = boost::any_cast<size_t>( ice_data[S_POP_PTS] );

      double d_lmin = log10( my_imf.getLowerMass() );

      double d_lmax = log10( my_imf.getUpperMass() );

      double delta = ( d_lmax - d_lmin ) / static_cast<double>( n );

      const double dm = delta / 10.;

      for( size_t i = 0 ; i <= n; ++i )
      {
        masses.push_back(
          pow( 10., d_lmin + delta * i )
        );
      }

      double y = 0.;

      boost::numeric::odeint::integrate_times(
        boost::numeric::odeint::make_controlled(
          1E-12,
          1E-12,
          stepper_type()
        ),
        my_rhs,
        y,
        masses,
        dm,
        my_dev
      );

      norm = v[v.size()-1].first;

      for( size_t i = 0; i < v.size(); i++ )
      {
        v[i].first /= norm;
      }

    }

    double getMass( double x )
    {
      if( x > v.back().first || x < v[0].first )
      {
        std::cerr << "Invalid input to deviate routine." << std::endl;
        exit( EXIT_FAILURE );
      }
      std::vector<std::pair<double, double> >::iterator it, it2;
      it = lower_bound( v.begin(), v.end(), std::make_pair( x, -1. ) );
      if( it == v.begin() ) return it->second;
      it2 = it;
      --it2;
      return
        it2->second +
        ( it->second - it2->second ) *
        ( x - it2->first ) /( it->first - it2->first );
    }

    double getDeviate( double m )
    {
      if( m > v.back().second || m < v[0].second )
      {
        std::cerr << "m = " << m <<
          " is invalid input to deviate routine." << std::endl;
        exit( EXIT_FAILURE );
      }
      for( size_t i = 0; i < v.size() - 1; i++ )
      {
        if( m >= v[i].second && m < v[i+1].second )
        {
          return
            v[i].first +
            ( v[i+1].first - v[i].first ) * ( m - v[i].second ) /
            ( v[i+1].second - v[i].second );
        }
      }
      return v[v.size()-1].first;
    }

    double getNorm() {return norm;}

    private:
      ice_data_t& ice_data; 
      std::vector<std::pair<double, double> > v;
      double norm;

};

//##############################################################################
// pop_hist().
//##############################################################################

class pop_hist
{

  public:
    pop_hist( ice_data_t& _ice_data ) : ice_data( _ice_data )
    {

      double d_lmin =
        log10( boost::any_cast<double>( ice_data[S_IMF_ML] ) );

      double d_lmax =
        log10( boost::any_cast<double>( ice_data[S_IMF_MU] ) );

      size_t n_bins = boost::any_cast<size_t>( ice_data[S_POP_PTS] );

      for( size_t i = 0; i <= n_bins; i++ )
      {
        lmass_bins.push_back(
          d_lmin + i * ( d_lmax - d_lmin ) / static_cast<double>( n_bins )
        );
        number.push_back( 0 );
      }

    }

    pop_hist& operator=( const pop_hist& p )
    {
      ice_data = p.ice_data;
      lmass_bins = p.lmass_bins;
      number = p.number;
      return *this;
    }

    pop_hist& operator+=( const pop_hist& p )
    {
      for( size_t i = 0; i < number.size(); i++ )
      {
        number[i] += p.number[i];
      }
      return *this;
    } 


    void update( const double mass, const int num )
    {
      size_t i_mass =
        std::lower_bound( lmass_bins.begin(), lmass_bins.end(), log10( mass ) )
        -
        lmass_bins.begin();
      if( i_mass < lmass_bins.size() )
        number[i_mass-1] += num;
    }

    size_t total()
    {
      return std::accumulate( number.begin(), number.end(), 0 );
    }

    std::vector<pop_tup_t>
    get_tuple()
    {

      std::vector<pop_tup_t> result;

      size_t tot = total();

      for( size_t i = 0; i < lmass_bins.size() - 1; i++ )
      {
        result.push_back(
          boost::make_tuple(
            pow( 10, lmass_bins[i] ),
            pow( 10, lmass_bins[i+1] ),
            number[i],
            static_cast<double>( number[i] ) / static_cast<double>( tot )
          )
        );
      }

      return result;

    }

  private:
    ice_data_t& ice_data;
    std::vector<double> lmass_bins;
    std::vector<size_t> number;

};

//##############################################################################
// get_star_pop_descriptions().
//##############################################################################

void
get_star_pop_descriptions( po::options_description& star_pop )
{

  try
  {

    star_pop.add_options()

    // Option for name of imf deviate file
    ( S_DEV_FILE, po::value<std::string>(),
      "Name of text file for IMF deviate output (default: not set)" )
    
    // Option for lower mass limit for imf
    ( S_IMF_ML, po::value<double>()->default_value( 0.01 ),
      "Lower limit of star mass for IMF" )
    
    // Option for lower mass limit for imf
    ( S_IMF_MU, po::value<double>()->default_value( 100. ),
      "Upper limit of star mass for IMF" )
    
    // Option for total mass file
    ( S_MASS_FILE, po::value<std::string>(),
    "Name of total mass output file" )
    
    // Option for number of imf points
    ( S_POP_PTS, po::value<size_t>()->default_value( 100 ),
      "Number of mass points for POP output" )
    
    // Option for name of star pop output file
    ( S_POP_FILE, po::value<std::string>(),
      "Name of text file for star population output (default: not set)" )

    ;
    
// Add checks on input.
  
  }
  catch( std::exception& e )
  {
    std::cerr << "Error: " << e.what() << "\n";
    exit( EXIT_FAILURE );
  }
  catch(...)
  {
    std::cerr << "Exception of unknown type!\n";
    exit( EXIT_FAILURE );
  }
}

//##############################################################################
// set_star_pop_data().
//##############################################################################

void
set_star_pop_data(
  ice_data_t& ice_data,
  po::variables_map& vmap
)
{

  if( vmap.count(S_DEV_FILE) )
  {
    ice_data[S_DEV_FILE] = vmap[S_DEV_FILE].as<std::string>();
  }

  if( vmap.count(S_POP_FILE) )
  {
    ice_data[S_POP_FILE] = vmap[S_POP_FILE].as<std::string>();
  }

  ice_data[S_IMF_ML] = vmap[S_IMF_ML].as<double>();

  ice_data[S_IMF_MU] = vmap[S_IMF_MU].as<double>();

  ice_data[S_POP_PTS] = vmap[S_POP_PTS].as<size_t>();

  if( vmap.count(S_MASS_FILE) )
  {
    ice_data[S_MASS_FILE] = vmap[S_MASS_FILE].as<std::string>();

    std::ofstream my_file;
    my_file.open(
      boost::any_cast<std::string>( ice_data[S_MASS_FILE] ).c_str()
    );
    my_file.close();
  }

  ice_data[S_LOW_MASS_STAR_MASS] = 0.;

}

//##############################################################################
// output_star_pop().
//##############################################################################

void
output_star_pop( ice_data_t& ice_data ) 
{

  static std::ios_base::openmode current_mode = std::ios_base::out;

  imf_deviate my_dev( ice_data );

  if( ice_data.find( S_POP_FILE ) != ice_data.end() )
  {

    if(
      boost::any_cast<
        boost::shared_ptr<pop_hist>
      >( ice_data[S_POP_HIST] )->total() > 0 
    )
    {

      std::ofstream my_file;
      my_file.open(
        boost::any_cast<std::string>( ice_data[S_POP_FILE] ).c_str(),
        current_mode
      );

      my_file << "t = " << boost::any_cast<double>( ice_data[nnt::s_TIME] ) <<
        std::endl;

      std::vector<pop_tup_t> t_output =
        boost::any_cast<
          boost::shared_ptr<pop_hist>
        >( ice_data[S_POP_HIST] )->get_tuple();

      my_file << 2 * t_output.size() << std::endl;

      BOOST_FOREACH( pop_tup_t t, t_output )
      {
        double m_av = ( t.get<0>() + t.get<1>() ) / 2.;
        double dphi = my_dev.getDeviate( t.get<1>() ) -
                      my_dev.getDeviate( t.get<0>() );
        my_file << t.get<0>() << "  " <<
                   t.get<2>() << "  " <<
                   t.get<3>() << "  " <<
                   t.get<3>() * m_av << "  " <<
                   dphi << "  " << m_av * dphi << std::endl;
        my_file << t.get<1>() << "  " <<
                   t.get<2>() << "  " <<
                   t.get<3>() << "  " <<
                   t.get<3>() * m_av << "  " << 
                   dphi << "  " << m_av * dphi << std::endl;
      }

      my_file << std::endl;

      my_file.close();

      current_mode = std::ios_base::app;

    }

  }

} 

//##############################################################################
// output_imf_deviate().
//##############################################################################

void
output_imf_deviate( ice_data_t& ice_data )
{

  imf_deviate imf_dev( ice_data );

  if( ice_data.find( S_DEV_FILE ) != ice_data.end() )
  {

    std::ofstream my_file;
    my_file.open(
      boost::any_cast<std::string>( ice_data[S_DEV_FILE] ).c_str()
    );

    std::vector<pop_tup_t> t_output =
      boost::any_cast<
        boost::shared_ptr<pop_hist>
      >( ice_data[S_POP_HIST] )->get_tuple();

    for( size_t i = 0; i <= t_output.size(); i++ )
    {
      double x = (double) i / (double) t_output.size();
      my_file << imf_dev.getMass( x ) << "  " << x << std::endl;
    }

    my_file.close();

  }

}

#endif  // STAR_POP_HPP
