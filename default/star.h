//////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header code to compute properties of stars.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <iostream>
#include <ctime>
#include <vector>
#include <algorithm>

#include <boost/tuple/tuple.hpp>
#include <boost/random/discrete_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/poisson_distribution.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/variate_generator.hpp>

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef ICE_STAR_H
#define ICE_STAR_H

//##############################################################################
// Types.
//##############################################################################

struct stats
{
  size_t count;
  double mean;
  double sigma;

  stats( size_t c, double m, double s ) : count( c ), mean( m ), sigma( s ){}

};

struct StarSystemGenerator
{
    StarSystemGenerator(){} 
    size_t operator()( boost::mt19937& gen )
    {
      double p[] = {0.5,0.5};
      boost::random::discrete_distribution<> dist( p );
      return dist( gen ) + 1;  // dist is zero-indexed--add 1
    }

};

class Star
{

  public:
    Star(
      stats _st,
      double d_formation_time,
      size_t _id,
      size_t _orig_zone_id,
      boost::tuple<int,int,int> t,
      boost::tuple<double,double,double> u
    ) :
      st( _st ),
      formation_time( d_formation_time ),
      id( _id ),
      orig_zone_id( _orig_zone_id ),
      x( t.get<0>() ),
      y( t.get<1>() ),
      z( t.get<2>() ),
      xFactor( u.get<0>() ),
      yFactor( u.get<1>() ),
      zFactor( u.get<2>() )
    {
      life_time = 1.e10 * D_YEAR / pow( st.mean, 2 );
      original_mass = st.mean;
      current_mass = original_mass;
      status = "star";
      end_time = formation_time + life_time;
    }

    bool operator < (const Star& s) const
    {
        return( getEndTime() > s.getEndTime() );
    }
    double getFormationTime() const { return formation_time; }
    double getOriginalMass() const { return original_mass; }
    double getMass() const { return current_mass; }
    size_t getID() const { return id; }
    size_t getOriginalZoneId() const { return orig_zone_id; }
    size_t getNumber() const { return st.count; }
    size_t getX() const { return x; }
    size_t getY() const { return y; }
    size_t getZ() const { return z; }
    double getXFactor() const { return xFactor; }
    double getYFactor() const { return yFactor; }
    double getZFactor() const { return zFactor; }
    void setMetallicity( double m ) { metallicity = m; }
    double getMetallicity() const { return metallicity; }
    double getEndTime() const { return end_time; }
    double getLifeTime() const { return life_time; }
    std::string getStatus() const { return status; }
    void updateCurrentMass( double mass ) { current_mass = mass; }
    void updateEndTime( double time ) { end_time += time; }
    void updateStatus( std::string s ) { status = s; }

  protected:
    stats st;
    double original_mass;
    double current_mass;
    double formation_time;
    double life_time;
    double end_time;
    std::string status;
    size_t id;
    size_t orig_zone_id;
    size_t x;
    size_t y;
    size_t z;
    double xFactor;
    double yFactor;
    double zFactor;
    double metallicity;
};

typedef boost::heap::fibonacci_heap<Star> star_heap_t;

class StarSystem
{

  public:
    StarSystem(){}
    void addStar( Star star ){ star_heap.push( star ); }
    Star getTopStar() const { return star_heap.top(); }
    void popTopStar() { star_heap.pop(); }
    star_heap_t getStarHeap() const { return star_heap; }
    size_t getNumberOfStars()
    {
      if( star_heap.empty() )
        return 0;
      else
        return star_heap.size();
    }
    bool operator < (const StarSystem& s) const
    {
        return( getTopStar().getEndTime() > s.getTopStar().getEndTime() );
    }
    std::vector<Star>
    getStarVector() const
    {
      std::vector<Star> v;

      for(
        star_heap_t::ordered_iterator it = star_heap.ordered_begin();
        it != star_heap.ordered_end();
        it++
      )
      { v.push_back( *it ); }

      return v;
    }


  private:
    star_heap_t star_heap;
    double life_time;
    std::string type;

};

typedef boost::heap::fibonacci_heap<StarSystem> heap_t;

#endif // ICE_STAR_H
