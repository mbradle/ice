//////////////////////////////////////////////////////////////////////////////
//  
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to compute links in multi-zone chemical evolution code.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <boost/assign.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/graph/iteration_macros.hpp>

//##############################################################################
// Check if already included.
//##############################################################################

#ifndef COS_WAVE_HALO_HPP
#define COS_WAVE_HALO_HPP

//##############################################################################
// Strings.
//##############################################################################

#define S_HOT_MIX  "hot_mix"
#define S_HALO_MIX  "halo_mix"
#define S_P  "p"
#define S_A  "A"
#define S_T  "T"
#define S_M  "m"

#define S_P_X  "p_x"
#define S_A_X  "A_x"
#define S_T_X  "T_x"
#define S_M_X  "m_x"

#define S_P_Y  "p_y"
#define S_A_Y  "A_y"
#define S_T_Y  "T_y"
#define S_M_Y  "m_y"

#define S_P_Z  "p_z"
#define S_A_Z  "A_z"
#define S_T_Z  "T_z"
#define S_M_Z  "m_z"

//##############################################################################
// Types.
//##############################################################################

typedef boost::tuple<int,int,int> int_tuple_t;

typedef boost::tuple<std::string,std::string,std::string> str_tuple_t;

typedef std::vector<boost::shared_ptr<double> > v_ptr_t;

//##############################################################################
// check_value().
//##############################################################################

int
check_value(
  int i,
  int j
)
{

  if( i < 0 )
  {
    return j;
  }
  else if( i > j )
  {
    return 0;
  } 
  
  return i;
}

//##############################################################################
// tuple_add().
//##############################################################################

struct tuple_add
{

  std::vector<int> n_max;

  tuple_add( std::vector<int> n ) : n_max( n ) {}

  int_tuple_t operator()( int_tuple_t lhs, const int_tuple_t& rhs )
  {
    lhs.get<0>() = check_value( lhs.get<0>() + rhs.get<0>(), n_max[0] );
    lhs.get<1>() = check_value( lhs.get<1>() + rhs.get<1>(), n_max[1] );
    lhs.get<2>() = check_value( lhs.get<2>() + rhs.get<2>(), n_max[2] );

    return lhs;
  }

};

//##############################################################################
// is_hot_zone().
//##############################################################################

bool
is_hot_zone( Libnucnet__Zone * p_zone )
{
  std::string s3 = Libnucnet__Zone__getLabel( p_zone, 3 );

  if( s3.find( "h" ) != std::string::npos )
  {
    return true;
  }
  else
  {
    return false;
  }
}

//##############################################################################
// is_halo_zone().
//##############################################################################

bool
is_halo_zone( Libnucnet__Zone * p_zone )
{
  std::string s1 = Libnucnet__Zone__getLabel( p_zone, 1 );

  if( s1.find( "halo" ) != std::string::npos )
  {
    return true;
  }
  else
  {
    return false;
  }
}

//##############################################################################
// set_zones().
//##############################################################################

void
set_zones(
  std::vector<nnt::Zone>& zones,
  ice_data_t& ice_data
)
{
  Libnucnet * p_nucnet = boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] );

  std::vector<nnt::Zone> new_zones;

  //==========================================================================
  // Add hot zones.
  //==========================================================================

  if( ice_data.find( S_HOT_MIX ) != ice_data.end() )
  { 
    BOOST_FOREACH( nnt::Zone zone, zones )
    {
      std::string s3 =
        std::string(
          Libnucnet__Zone__getLabel( zone.getNucnetZone(), 3 )
        ) + "h";

      nnt::Zone new_zone;

      new_zone.setNucnetZone(
        Libnucnet__Zone__new(
          Libnucnet__getNet( p_nucnet ),
          Libnucnet__Zone__getLabel( zone.getNucnetZone(), 1 ),
          Libnucnet__Zone__getLabel( zone.getNucnetZone(), 2 ),
          s3.c_str()
        )
      );

      new_zone.updateProperty(
        nnt::s_ZONE_MASS,
        0.
      );

      gsl_vector * p_abunds =
        Libnucnet__Zone__getAbundances( zone.getNucnetZone() );

      Libnucnet__Zone__updateAbundances( new_zone.getNucnetZone(), p_abunds );

      gsl_vector_free( p_abunds );

      new_zones.push_back( new_zone );
    }

  }

  //==========================================================================
  // Add halo zone.
  //==========================================================================

  if( ice_data.find( S_HALO_MIX ) != ice_data.end() )
  {
    //==========================================================================
    // Set base masses.
    //==========================================================================

    BOOST_FOREACH( nnt::Zone zone, zones )
    {
      zone.updateProperty(
        nnt::s_ZONE_MASS,
        0.
      );
    }

    nnt::Zone halo_zone;

    halo_zone.setNucnetZone(
      Libnucnet__Zone__new(
        Libnucnet__getNet( p_nucnet ),
          "halo",
          "0",
          "0"
      )
    );
    
    halo_zone.updateProperty(
      nnt::s_ZONE_MASS,
      boost::any_cast<double>( ice_data[S_TOTAL_MASS] ) 
    );

    gsl_vector * p_abunds =
      Libnucnet__Zone__getAbundances( zones[0].getNucnetZone() );

    Libnucnet__Zone__updateAbundances( halo_zone.getNucnetZone(), p_abunds );

    gsl_vector_free( p_abunds );

    new_zones.push_back( halo_zone );
  }
  else
  { 
    //==========================================================================
    // Set base masses.
    //==========================================================================

    BOOST_FOREACH( nnt::Zone zone, zones )
    {
      zone.updateProperty(
        nnt::s_ZONE_MASS,
        boost::any_cast<double>( ice_data[S_TOTAL_MASS] ) / 
          static_cast<double>( zones.size() )
      );
    }
  }
    
  //==========================================================================
  // Add new zones to nucnet.
  //==========================================================================

  BOOST_FOREACH( nnt::Zone new_zone, new_zones )
  {
    Libnucnet__addZone( p_nucnet, new_zone.getNucnetZone() );
    zones.push_back( new_zone );
  }

}

//##############################################################################
// get_injecta_zone().
//##############################################################################

nnt::Zone
get_injecta_zone(
  ice_data_t& ice_data,
  const StarSystem& star_system
)
{

  nnt::Zone zone;
  Star star = star_system.getTopStar();
  std::string s3 = boost::lexical_cast<std::string>( star.getZ() );

  if( ice_data.find( S_HOT_MIX ) != ice_data.end() )
  {
    s3 += "h";
  }

  zone.setNucnetZone(
    Libnucnet__getZoneByLabels(
      boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] ),
      boost::lexical_cast<std::string>( star.getX() ).c_str(),
      boost::lexical_cast<std::string>( star.getY() ).c_str(),
      s3.c_str()
    )
  );

  return zone;

}

//##############################################################################
// compute_rate_ratio().
//##############################################################################

double
compute_rate_ratio(
  ice_data_t& ice_data,
  size_t i_coord,
  size_t i_index,
  double& t,
  std::vector<int>& n_max
)
{  
  double d_P, d_A, d_T, d_M;  

  if(
    (boost::any_cast<v_ptr_t>( ice_data[S_P] ))[i_coord] &&
    (boost::any_cast<v_ptr_t>( ice_data[S_A] ))[i_coord] &&
    (boost::any_cast<v_ptr_t>( ice_data[S_T] ))[i_coord] &&
    (boost::any_cast<v_ptr_t>( ice_data[S_M] ))[i_coord]
  )
  {
    d_P = *(boost::any_cast<v_ptr_t>( ice_data[S_P] ))[i_coord];
    d_A = *(boost::any_cast<v_ptr_t>( ice_data[S_A] ))[i_coord];
    d_T = *(boost::any_cast<v_ptr_t>( ice_data[S_T] ))[i_coord];
    d_M = *(boost::any_cast<v_ptr_t>( ice_data[S_M] ))[i_coord];
  }
  else
  {
    std::cerr << "Data not available for cos wave." << std::endl;
    exit( EXIT_FAILURE );
  }
  
  return
    1.
    +
    d_A
    *
    pow(
      cos(
        d_M 
        *
        M_PI
        *
        (
          ( t / d_T ) - ( (double) i_index / (double) ( n_max[i_coord] + 1 ) )
        )
      ),
      d_P
    ); 

}

//##############################################################################
// get_user_defined_descriptions().
//##############################################################################

void
get_user_defined_descriptions( po::options_description& user )
{

  try
  {

    po::options_description general( "\nGeneral options" );

    general.add_options()
    // Option for value of hot-zone-mixing timescale in years
    ( S_HOT_MIX, po::value<double>(),
      "hot-zone-mixing timescale in years (optional)\n" )
  
    // Option for value of halo-zone-mixing timescale in years
    ( S_HALO_MIX, po::value<double>(),
      "halo-zone-mixing timescale in years (optional)\n" );
  
    po::options_description X( "\nX options" );

    X.add_options()

    // Option for value of power of cosine wave in x-direction
    (
      S_P_X,
      po::value<double>(), "power of cosine wave in x-direction (required if"
      " neither p_y nor p_z is set)\n"
    )
  
    // Option for value of amplitude of cosine wave in x-direction
    (
      S_A_X,
      po::value<double>(), "amplitude of cosine wave in x-direction "
      "(required if neither A_y nor A_z is set)\n"
    )
  
    // Option for value of period of cosine wave in x-direction
    (
      S_T_X,
      po::value<double>(),
      "period of cosine wave in x-direction (required "
      "if neither T_y nor T_z is set)\n"
    )
  
    // Option for value of multiplier of cosine wave in x-direction
    (
      S_M_X,
      po::value<size_t>(),
      "multiplier of cosine wave in x-direction (required "
      "if neither m_y nor m_z is set)\n"
    );
  
    po::options_description Y( "\nY options" );

    Y.add_options()

    // Option for value of power of cosine wave in y-direction
    (
      S_P_Y,
      po::value<double>(),
      "power of cosine wave in y-direction (required if"
      " neither p_x nor p_z is set)\n"
    )
  
    // Option for value of amplitude of cosine wave in y-direction
    (
      S_A_Y,
      po::value<double>(),
      "amplitude of cosine wave in y-direction "
      "(required if neither A_x nor A_z is set)\n"
    )
  
    // Option for value of period of cosine wave in y-direction
    (
      S_T_Y,
      po::value<double>(),
      "period of cosine wave in y-direction (required "
      "if neither T_x nor T_z is set)\n"
    )
  
    // Option for value of multiplier of cosine wave in y-direction
    (
      S_M_Y,
      po::value<size_t>(),
      "multiplier of cosine wave in y-direction (required "
      "if neither m_x nor m_z is set)\n"
    );
  
    po::options_description Z( "\nZ options" );

    Z.add_options()

    // Option for value of power of cosine wave in z-direction
    (
      S_P_Z,
      po::value<double>(),
      "power of cosine wave in z-direction (required if"
      " neither p_x nor p_y is set)\n"
    )
  
    // Option for value of amplitude of cosine wave in z-direction
    (
      S_A_Z,
      po::value<double>(),
      "amplitude of cosine wave in z-direction "
      "(required if neither A_x nor A_y is set)\n"
    )
  
    // Option for value of period of cosine wave in z-direction
    (
      S_T_Z,
      po::value<double>(),
      "period of cosine wave in z-direction (required "
      "if neither T_x nor T_y is set)"
    )

    // Option for value of multiplier of cosine wave in z-direction
    (
      S_M_Z,
      po::value<size_t>(),
      "multiplier of cosine wave in z-direction (required "
      "if neither m_x nor m_y is set)\n"
    );
  
    user.add( general ).add( X ).add( Y ).add( Z );

// Add checks on input.
  
  }
  catch( std::exception& e )
  {
    std::cerr << "Error: " << e.what() << "\n";
    exit( EXIT_FAILURE );
  }
  catch(...)
  {
    std::cerr << "Exception of unknown type!\n";
    exit( EXIT_FAILURE );
  }
}

//##############################################################################
// set_user_defined_options().
//##############################################################################

void
set_user_defined_options( po::variables_map& vmap, ice_data_t& ice_data )
{

  v_ptr_t p, a, t, m;

  const std::vector<std::string> p_params =
    boost::assign::list_of(S_P_X)(S_P_Y)(S_P_Z);

  const std::vector<std::string> a_params =
    boost::assign::list_of(S_A_X)(S_A_Y)(S_A_Z);

  const std::vector<std::string> t_params =
    boost::assign::list_of(S_T_X)(S_T_Y)(S_T_Z);

  const std::vector<std::string> m_params =
    boost::assign::list_of(S_M_X)(S_M_Y)(S_M_Z);

  BOOST_FOREACH( std::string s_param, p_params )
  {
    boost::shared_ptr<double> ptr( new double );
    if( vmap.count( s_param ) )
    {
      *ptr = vmap[s_param].as<double>();
    }
    else
    {
      ptr.reset();
    }
    p.push_back( ptr );
  }
  ice_data[S_P] = p;

  BOOST_FOREACH( std::string s_param, a_params )
  {
    boost::shared_ptr<double> ptr( new double );
    if( vmap.count( s_param ) )
    {
      *ptr = vmap[s_param].as<double>();
    }
    else
    {
      ptr.reset();
    }
    a.push_back( ptr );
  }
  ice_data[S_A] = a;

  BOOST_FOREACH( std::string s_param, t_params )
  {
    boost::shared_ptr<double> ptr( new double );
    if( vmap.count( s_param ) )
    {
      *ptr = vmap[s_param].as<double>();
    }
    else
    {
      ptr.reset();
    }
    t.push_back( ptr );
  }
  ice_data[S_T] = t;

  BOOST_FOREACH( std::string s_param, m_params )
  {
    boost::shared_ptr<double> ptr( new double );
    if( vmap.count( s_param ) )
    {
      *ptr = static_cast<double>( vmap[s_param].as<size_t>() );
    }
    else
    {
      ptr.reset();
    }
    m.push_back( ptr );
  }
  ice_data[S_M] = m;

  if( vmap.count( S_HOT_MIX ) )
  {
    ice_data[S_HOT_MIX] = vmap[S_HOT_MIX].as<double>() * D_YEAR;
  }
  if( vmap.count( S_HALO_MIX ) )
  {
    ice_data[S_HALO_MIX] = vmap[S_HALO_MIX].as<double>() * D_YEAR;
  }

}

//##############################################################################
// get_star_forming_zones().
//##############################################################################

std::vector<nnt::Zone>
get_star_forming_zones(
  Libnucnet * p_nucnet
)
{

  std::vector<nnt::Zone> zones;

  BOOST_FOREACH( nnt::Zone zone, nnt::make_zone_list( p_nucnet ) )
  {
    if( 
      !is_hot_zone( zone.getNucnetZone() ) && 
      !is_halo_zone( zone.getNucnetZone() ) 
    ) 
    {
      zones.push_back( zone );
    }
  }

  return zones;

}

//##############################################################################
// set_links().
//##############################################################################

void
set_links( 
  user::zone_link_graph_t& g, 
  std::vector<nnt::Zone>& zones,
  ice_data_t& ice_data
)
{
  typedef user::zone_link_graph_t Graph;
  Graph::vertex_descriptor v_to;
  Graph::edge_descriptor e;
  user::vertex_multi_index vm;
  user::fill_multi_zone_vertex_hash( g, vm );

  bool e_add;

  std::vector<int> labels_x, labels_y, labels_z;
  std::vector<int> n_max;
  std::vector<int_tuple_t> delta; 

  double t = boost::any_cast<double>( ice_data[nnt::s_TIME] );
  
  BGL_FORALL_VERTICES( v, g, Graph )
  {
    if(
      !is_hot_zone( g[v].getNucnetZone() ) && 
      !is_halo_zone( g[v].getNucnetZone() ) 
    )
    {
      labels_x.push_back (
        boost::lexical_cast<int>(
          Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 1 )
        ) 
      );
      labels_y.push_back (
        boost::lexical_cast<int>(
          Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 2 )
        ) 
      );
      labels_z.push_back (
        boost::lexical_cast<int>(
          Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 3 )
        ) 
      );
    }
  }

  n_max.push_back( *std::max_element( labels_x.begin(), labels_x.end() ) );
  n_max.push_back( *std::max_element( labels_y.begin(), labels_y.end() ) );
  n_max.push_back( *std::max_element( labels_z.begin(), labels_z.end() ) );

  tuple_add my_tuple_add( n_max );

  delta.push_back( int_tuple_t( 1, 0, 0 ) );
  delta.push_back( int_tuple_t( -1, 0, 0 ) );
  delta.push_back( int_tuple_t( 0, 1, 0 ) );
  delta.push_back( int_tuple_t( 0, -1, 0 ) );
  delta.push_back( int_tuple_t( 0, 0, 1 ) );
  delta.push_back( int_tuple_t( 0, 0, -1 ) );

  BGL_FORALL_VERTICES( v, g, Graph )
  {
    str_tuple_t
      source_tuple(
        std::string( Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 1 ) ),
        std::string( Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 2 ) ),
        std::string( Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 3 ) )
      );

    if( 
      !is_hot_zone( g[v].getNucnetZone() ) && 
      !is_halo_zone( g[v].getNucnetZone() ) 
    )
    {
      for( size_t i = 0; i < delta.size(); i++ )
      {

        int_tuple_t target_tuple;
        std::vector<int> v_tar;

        int_tuple_t
          source_int_tuple(
            boost::lexical_cast<int>( source_tuple.get<0>() ),
            boost::lexical_cast<int>( source_tuple.get<1>() ),
            boost::lexical_cast<int>( source_tuple.get<2>() )
          );

        target_tuple = my_tuple_add( source_int_tuple, delta[i] );

        v_tar.push_back( target_tuple.get<0>() );
        v_tar.push_back( target_tuple.get<1>() );
        v_tar.push_back( target_tuple.get<2>() );

        if( target_tuple != source_int_tuple )
        {

          v_to =
            user::get_vertex_from_multi_zone_hash(
              vm,
              boost::lexical_cast<std::string>( target_tuple.get<0>() ),
              boost::lexical_cast<std::string>( target_tuple.get<1>() ),
              boost::lexical_cast<std::string>( target_tuple.get<2>() )
            );

          if( v_to != Graph::null_vertex() )
          {
            boost::tie( e, e_add ) = boost::add_edge( v, v_to, g );

            g[e].setWeight( 
              compute_rate_ratio( 
                ice_data,
                i/2,
                v_tar[i/2],
                t,
                n_max
              )
              *
              ( 1.  / boost::any_cast<double>( ice_data[S_CLOUD_MIX] ) ) 
            );
          }
        }
      }
    }
    else if( is_hot_zone( g[v].getNucnetZone() ) )
    {
      v_to =
        user::get_vertex_from_multi_zone_hash(
          vm,
          source_tuple.get<0>(),
          source_tuple.get<1>(),
          (source_tuple.get<2>()).substr( 0, 1 )
        );

      if( v_to != Graph::null_vertex() )
      {
        if( ice_data.find(S_HOT_MIX) != ice_data.end() )
        {
          boost::tie( e, e_add ) = boost::add_edge( v, v_to, g );

          g[e].setWeight( 
            1. / boost::any_cast<double>( ice_data[S_HOT_MIX] )
          );
        }
      }
    }
    else if( is_halo_zone( g[v].getNucnetZone() ) )
    {
      size_t n_to = (n_max[0]+1) * (n_max[1]+1) * (n_max[2]+1);
      BGL_FORALL_VERTICES( v_to, g, Graph )
      {
        if( v_to != Graph::null_vertex() )
        {
          if( 
            !is_hot_zone( g[v_to].getNucnetZone() ) && 
            !is_halo_zone( g[v_to].getNucnetZone() ) 
          )
          {
            if( ice_data.find(S_HALO_MIX) != ice_data.end() )
            {
              boost::tie( e, e_add ) = boost::add_edge( v, v_to, g );

              g[e].setWeight( 
                1. /
                (
                  (double) n_to *
                  boost::any_cast<double>( ice_data[S_HALO_MIX] )
                )
              );
            }
          }
        }
      }
    }
    else
    {
      std::cerr << "Invalid zone." << std::endl;
      exit( EXIT_FAILURE );
    }
  }
}

#endif  // COS_WAVE_HALO_HPP
