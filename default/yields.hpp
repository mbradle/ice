////////////////////////////////////////////////////////////////////////////////
//  
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header for standard yields code.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <algorithm>
#include <functional>
#include <iostream>
#include <set>
#include <map>

#include "nnt/iter.h"

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef YIELDS_HPP
#define YIELDS_HPP

//##############################################################################
// Strings.
//##############################################################################

#define S_YIELDS_NUCNET       "yields nucnet"
#define S_SOLAR_METALLICITY   "solar_metallicity"
#define S_YIELDS_PUSH_TIME    "yields_push_time"
#define S_R_PROCESS           "r process"
#define S_SN_R_PROCESS_MASS   "sn_r_proc_mass"
#define S_REMNANT_MASS        "remnant mass"
#define S_TOTAL_EJECTED_MASS  "total ejected mass"
#define S_ZERO_YIELDS         "zero_yields"

//##############################################################################
// Types.
//##############################################################################

typedef std::pair<double,double> yield_key_t;

std::map<yield_key_t, double> remnant_mass_map;

std::map<std::string, double> other_remnant_mass_map;
std::map<std::string, double> other_ejected_mass_map;

std::map<yield_key_t, Libnucnet__Zone *> yield_map;
std::map<std::string, Libnucnet__Zone *> other_yield_map;

std::vector<double> v_mass, v_metallicity;

//##############################################################################
// get_yields_descriptions().
//##############################################################################

void
get_yields_descriptions( po::options_description& yields )
{

  try
  {

    yields.add_options()

    // Option for push time of yields
      ( S_YIELDS_PUSH_TIME, po::value<double>(),
        "Push time for yields in years (default: 0)"
      )
    
    // Option for solar metallicity
      ( S_SOLAR_METALLICITY, po::value<double>()->default_value( 0.02, "0.02" ),
        "Solar metallicity (default: 0.02)"
      )

    // Option to zero out yields
      ( S_ZERO_YIELDS, po::value<std::string>()->default_value( "no" ),
        "Zero the yields"
      )

    // Option to zero out yields
      ( S_SN_R_PROCESS_MASS, po::value<double>()->default_value( 0, "0" ),
        "Mass of r process from a supernova"
      )

    ;

// Add checks on input.
  
  }
  catch( std::exception& e )
  {
    std::cerr << "Error: " << e.what() << "\n";
    exit( EXIT_FAILURE );
  }
  catch(...)
  {
    std::cerr << "Exception of unknown type!\n";
    exit( EXIT_FAILURE );
  }
}

//##############################################################################
// set_yields().
//##############################################################################

void
set_yields( ice_data_t& ice_data )
{

  std::set<double> mass, metallicity;

  Libnucnet * p_nucnet =
    boost::any_cast<Libnucnet *>( ice_data[S_YIELDS_NUCNET] );

  BOOST_FOREACH( nnt::Zone zone, nnt::make_zone_list( p_nucnet ) )
  {

    std::string s_type = Libnucnet__Zone__getLabel( zone.getNucnetZone(), 1 );

    if( s_type == S_STAR )
    {

      double d_mass = zone.getProperty<double>( S_INITIAL_MASS );
      double d_metallicity = zone.getProperty<double>( S_INITIAL_METALLICITY );
      double d_remnant_mass = zone.getProperty<double>( S_REMNANT_MASS );

      mass.insert( d_mass );
      metallicity.insert( d_metallicity );

      yield_key_t p( d_metallicity, d_mass );

      if( remnant_mass_map.find( p ) != remnant_mass_map.end() )
      {
         std::cerr << "Duplicate yield entry!" << std::endl;
      }

      remnant_mass_map[p] = d_remnant_mass;
      yield_map[p] = zone.getNucnetZone();

    }
    else
    {

      if( zone.hasProperty( S_REMNANT_MASS ) )
      {

        double d_remnant_mass = zone.getProperty<double>( S_REMNANT_MASS );

        if(
          !(
             other_remnant_mass_map.insert(
               std::make_pair(s_type, d_remnant_mass )
             )
          ).second
        )
        {
           std::cerr << "Duplicate yield entry!" << std::endl;
        }

      }

      if( zone.hasProperty( S_TOTAL_EJECTED_MASS ) )
      {

        double d_ejected_mass =
          zone.getProperty<double>( S_TOTAL_EJECTED_MASS );

        if(
          !(
             other_ejected_mass_map.insert(
               std::make_pair(s_type, d_ejected_mass )
             )
          ).second
        )
        {
           std::cerr << "Duplicate yield entry!" << std::endl;
        }

      }

      other_yield_map[s_type] = zone.getNucnetZone();

    }

  }

  BOOST_FOREACH( double m, mass )
  {
    v_mass.push_back( m );
  }

  BOOST_FOREACH( double x, metallicity )
  {
    v_metallicity.push_back( x );
  }

  bool all_yields_present = true;

  BOOST_FOREACH( double x, metallicity )
  {
    BOOST_FOREACH( double m, mass )
    {
      if( yield_map.find( std::make_pair( x, m ) ) == yield_map.end() )
      {
        std::cerr << "Entry (" << x << "," << m << ") not present" << std::endl;
        all_yields_present = false;
      }
    }
  }

  if( !all_yields_present ) exit( EXIT_FAILURE );

  double d_m_r_sn = boost::any_cast<double>( ice_data[S_SN_R_PROCESS_MASS] );

  if( other_yield_map.find( S_R_PROCESS ) != other_yield_map.end() )
  {

    Libnucnet__Zone * p_r = other_yield_map[S_R_PROCESS];

    gsl_vector * p_a_r = Libnucnet__Zone__getMassFractions( p_r );
    gsl_vector_scale( p_a_r, d_m_r_sn );

    BOOST_FOREACH( double x, metallicity )
    {
      BOOST_FOREACH( double m, mass )
      {
        if( m > 20 )
        {
          Libnucnet__Zone * p_star = yield_map[std::make_pair( x, m )];
          double d_ejected_mass = m - remnant_mass_map[std::make_pair( x, m )];
          if( d_ejected_mass < 0 )
          {
            std::cerr << "Ejected mass less than zero." << std::endl;
            exit( EXIT_FAILURE );
          }
          gsl_vector * p_a_s = Libnucnet__Zone__getMassFractions( p_star );
          gsl_vector_scale( p_a_s, d_ejected_mass ); 
          gsl_vector_add( p_a_s, p_a_r );
          gsl_vector_scale( p_a_s, 1. / ( d_m_r_sn + d_ejected_mass ) );
          Libnucnet__Zone__updateMassFractions( p_star, p_a_s );
          gsl_vector_free( p_a_s );
        }
      }
    }

    gsl_vector_free( p_a_r );

  }
            
}

//##############################################################################
// set_yield_data().
//##############################################################################

void
set_yield_data(
  const char * s_file,
  ice_data_t& ice_data,
  po::variables_map& vmap
)
{

  //==========================================================================
  // Get yields.
  //==========================================================================

  ice_data[S_YIELDS_NUCNET] =
    nnt::create_network_copy(
      boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] )
    );

  Libnucnet__assignZoneDataFromXml(
    boost::any_cast<Libnucnet *>( ice_data[S_YIELDS_NUCNET] ),
    s_file,
    NULL
  );

  if( vmap.count( S_YIELDS_PUSH_TIME ) )
  {
    user::push_abundances_to_daughters(
      boost::any_cast<Libnucnet *>( ice_data[S_YIELDS_NUCNET] ),
      1. / ( vmap[S_YIELDS_PUSH_TIME].as<double>() * D_YEAR )
    );
  }

  ice_data[S_SN_R_PROCESS_MASS] = vmap[S_SN_R_PROCESS_MASS].as<double>();

  set_yields( ice_data );

  if( vmap[S_ZERO_YIELDS].as<std::string>() == "yes" )
  {
    BOOST_FOREACH(
      nnt::Zone zone,
      nnt::make_zone_list(
        boost::any_cast<Libnucnet *>( ice_data[S_YIELDS_NUCNET] )
      )
    )
    {
      gsl_vector * p_abunds =
        Libnucnet__Zone__getAbundances( zone.getNucnetZone() );
      gsl_vector_set_zero( p_abunds );
      Libnucnet__Zone__updateAbundances( zone.getNucnetZone(), p_abunds );
      gsl_vector_free( p_abunds );
    }
  }

  ice_data[S_SOLAR_METALLICITY] = vmap[S_SOLAR_METALLICITY].as<double>();

}

//##############################################################################
// yields_cleanup().
//##############################################################################

void
yields_cleanup( ice_data_t& ice_data )
{
  Libnucnet__free( boost::any_cast<Libnucnet *>( ice_data[S_YIELDS_NUCNET] ) );
}

//##############################################################################
// get_yield_data().
//##############################################################################

boost::tuple<gsl_vector *, double, double>
get_yield_data( ice_data_t& ice_data, const StarSystem& star_system )
{

  std::vector<Star> v = star_system.getStarVector();

  if( v[0].getStatus() == S_STAR )
  {

    if( v_mass.empty() || v[0].getMass() < v_mass[0] )
    {

      Libnucnet__Zone * p_zone =
        Libnucnet__getZoneByLabels(
          boost::any_cast<Libnucnet *>( ice_data[S_ZONE_HISTORY_NUCNET] ),
          boost::lexical_cast<std::string>(
            v[0].getOriginalZoneId()
          ).c_str(),
          "0",
          "0"
        );

      double remnant_mass = GSL_MIN( v[0].getMass(), 0.8 );

      // Add s-process here.

      return
        boost::make_tuple(
          Libnucnet__Zone__getMassFractions( p_zone ),
          v[0].getMass() - remnant_mass,
          remnant_mass
        );
    }

    double scaled_metallicity =
      v[0].getMetallicity() /
      boost::any_cast<double>( ice_data[S_SOLAR_METALLICITY] );

    std::vector<double>::iterator low;

    low = std::lower_bound(
      v_metallicity.begin(), v_metallicity.end(), scaled_metallicity
    );

    size_t i_x = low - v_metallicity.begin();

    if( i_x >= v_metallicity.size() )
    {
      std::cerr << "Metallicity exceeds highest table value." << std::endl;
      exit( EXIT_FAILURE );
    }
    else if(
      i_x == v_metallicity.size() - 1 ||
      scaled_metallicity != v_metallicity[i_x]
    )
    {
      i_x -= 1;
    }

    low = std::lower_bound(
      v_mass.begin(), v_mass.end(), v[0].getMass()
    );

    size_t i_m = low - v_mass.begin();

    if( i_m >= v_mass.size() )
    {
      std::cerr << "Stellar mass exceeds highest table value." << std::endl;
      exit( EXIT_FAILURE );
    }
    else if(
      i_m == v_mass.size() - 1 ||
      v[0].getMass() != v_mass[i_m]
    )
    {
      i_m -= 1;
    }

    double x = scaled_metallicity;
    double y = v[0].getMass();

    double x1 = v_metallicity[i_x];
    double x2 = v_metallicity[i_x+1];

    double y1 = v_mass[i_m];
    double y2 = v_mass[i_m+1];

    double d = ( x2 - x1 ) * ( y2 - y1 );

    double f11 = ( y2 - y ) * ( x2 - x ) / d;
    double f21 = ( y2 - y ) * ( x - x1 ) / d;
    double f12 = ( y - y1 ) * ( x2 - x ) / d;
    double f22 = ( y - y1 ) * ( x - x1 ) / d;

    gsl_vector * p_11 =
      Libnucnet__Zone__getMassFractions( yield_map[std::make_pair( x1, y1 )] );
    gsl_vector * p_21 =
      Libnucnet__Zone__getMassFractions( yield_map[std::make_pair( x2, y1 )] );
    gsl_vector * p_12 =
      Libnucnet__Zone__getMassFractions( yield_map[std::make_pair( x1, y2 )] );
    gsl_vector * p_22 =
      Libnucnet__Zone__getMassFractions( yield_map[std::make_pair( x2, y2 )] );

    gsl_vector_scale( p_11, f11 ); 
    gsl_vector_scale( p_21, f21 ); 
    gsl_vector_scale( p_12, f12 ); 
    gsl_vector_scale( p_22, f22 ); 

    size_t i_size = p_11->size;
    gsl_vector * p_result = gsl_vector_calloc( i_size );

    gsl_vector_add( p_result, p_11 );
    gsl_vector_add( p_result, p_21 );
    gsl_vector_add( p_result, p_12 );
    gsl_vector_add( p_result, p_22 );

    gsl_vector_free( p_11 );
    gsl_vector_free( p_21 );
    gsl_vector_free( p_12 );
    gsl_vector_free( p_22 );

    double remnant_mass =
      remnant_mass_map[std::make_pair( x1, y1 )] * f11
      +
      remnant_mass_map[std::make_pair( x2, y1 )] * f21
      +
      remnant_mass_map[std::make_pair( x1, y2 )] * f12
      +
      remnant_mass_map[std::make_pair( x2, y2 )] * f22;

    return
      boost::make_tuple(
        p_result, v[0].getMass() - remnant_mass, remnant_mass
      );

  }
  else if( v[0].getStatus() == S_WHITE_DWARF && v[1].getStatus() == S_STAR )
  {

    double remnant_mass = other_remnant_mass_map[v[0].getStatus()];
    return
      boost::make_tuple(
        Libnucnet__Zone__getMassFractions( other_yield_map[v[0].getStatus()] ),
        v[0].getMass() - remnant_mass,
        remnant_mass
      );

  }
  else if(
    v[0].getStatus() == S_NEUTRON_STAR &&
    v[1].getStatus() == S_NEUTRON_STAR
  )
  {

std::cout << "NS-NS merger!" << std::endl;

    double ejected_mass = other_ejected_mass_map[S_R_PROCESS];

    return
      boost::make_tuple(
        Libnucnet__Zone__getMassFractions( other_yield_map[S_R_PROCESS] ),
        ejected_mass,
        v[0].getMass() + v[1].getMass() - ejected_mass
      );

  }
  else
  {
    BOOST_FOREACH( Star s, v )
    {
      std::cerr << s.getStatus() << "  ";
    }
    std::cerr << "\nNo such yield\n";
    exit( EXIT_FAILURE );
  }

}

#endif  // YIELDS_HPP
