////////////////////////////////////////////////////////////////////////////////
// 
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Utility routines for multi-zone chemical evolution code.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include "ice_utilities.h"

#include "default/star_pop.hpp"
#include "default/yields.hpp"
#include "default/remnants.hpp"
#include "default/cos_wave_halo.hpp"

//##############################################################################
// get_zone_labels_as_tuple().
//##############################################################################

pos_t
get_zone_labels_as_integer_tuple( nnt::Zone& zone )
{

  Libnucnet__Zone * p_zone = zone.getNucnetZone();

  return
    boost::make_tuple(
      boost::lexical_cast<int>( Libnucnet__Zone__getLabel( p_zone, 1 ) ),
      boost::lexical_cast<int>( Libnucnet__Zone__getLabel( p_zone, 2 ) ),
      boost::lexical_cast<int>( Libnucnet__Zone__getLabel( p_zone, 3 ) )
    );

}
  
//##############################################################################
// set_history_zones().
//##############################################################################

void
set_history_zones( ice_data_t& ice_data )
{

  Libnucnet * p_history =
    boost::any_cast<Libnucnet *>( ice_data[S_ZONE_HISTORY_NUCNET] );

  boost::shared_ptr<zone_id_map_t> p_map =
    boost::any_cast<boost::shared_ptr<zone_id_map_t> >(
      ice_data[S_ZONE_ID_MAP]
    );

  p_map->clear();

  BOOST_FOREACH(
    nnt::Zone zone,
    get_star_forming_zones(
      boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] )
    )
  )
  {
    nnt::write_xml( p_history, zone.getNucnetZone() );
    p_map->insert(
      std::make_pair( zone, Libnucnet__getNumberOfZones( p_history ) )
    );
  }

std::cout << Libnucnet__getNumberOfZones( p_history ) << "  " <<
             p_map->size() << std::endl;

}

//##############################################################################
// update_history_zones().
//##############################################################################

void
update_history_zones( ice_data_t& ice_data )
{

  ice_data[S_HISTORY_DTIME] =
    boost::any_cast<double>( ice_data[S_HISTORY_DTIME] ) +
    boost::any_cast<double>( ice_data[nnt::s_DTIME] );

  if(
    Libnucnet__getNumberOfZones(
      boost::any_cast<Libnucnet *>( ice_data[S_ZONE_HISTORY_NUCNET] )
    ) > 0 &&
    boost::any_cast<size_t>( ice_data[S_STEP] ) %
    boost::any_cast<size_t>( ice_data[S_HISTORY_STEPS] ) == 0
  )
  {

    user::decay_abundances(
      boost::any_cast<Libnucnet *>( ice_data[S_ZONE_HISTORY_NUCNET] ),
      boost::any_cast<double>( ice_data[S_HISTORY_DTIME] )
    );

    set_history_zones( ice_data );

    ice_data[S_HISTORY_DTIME] = 0.;    // Reset

  }

}

//##############################################################################
// at_option_parser().
//##############################################################################

std::pair<std::string,std::string> at_option_parser( std::string const& s )
{
  if ( '@' == s[0] )
    return std::make_pair( std::string( "response-file" ), s.substr( 1 ) );
  else
    return std::pair<std::string,std::string>();
}

//##############################################################################
// program_options().
//##############################################################################

void 
program_options( 
  po::variables_map& vmap, 
  po::options_description& help, 
  po::options_description& general, 
  po::options_description& output,
  po::options_description& yields,
  po::options_description& remnants,
  po::options_description& star_pop,
  po::options_description& graph,
  po::options_description& user, 
  po::options_description& all 
) 
{
  const std::string& s = vmap["program_options"].as<std::string>();

  if( s == "help" ) 
  {
    std::cout << help << std::endl;
  } 
  else if( s == "general" ) 
  {
    std::cout << general << std::endl;
  } 
  else if( s == "output" ) 
  {
    std::cout << output << std::endl;
  } 
  else if( s == "yields" ) 
  {
    std::cout << yields << std::endl;
  } 
  else if( s == "remnants" ) 
  {
    std::cout << remnants << std::endl;
  } 
  else if( s == "star_pop" ) 
  {
    std::cout << star_pop << std::endl;
  } 
  else if( s == "graph" ) 
  {
    std::cout << graph << std::endl;
  } 
  else if( s == "user" ) 
  {
    std::cout << user << std::endl;
  } 
  else if( s == "all" ) 
  {
    std::cout << all << std::endl;
  } 
  else 
  {
    std::cout << "\nUnknown options_description '" << s << "' in the "
    "--program_options option\n\n";
    
    exit( EXIT_FAILURE );
  }

  exit( EXIT_SUCCESS );
}

//##############################################################################
// response_file().
//##############################################################################

po::variables_map 
response_file( 
  po::variables_map& vmap,  
  po::options_description& all 
)
{
  // Load the file and tokenize it
  std::ifstream ifs( vmap["response-file"].as<std::string>().c_str() );
  if( !ifs )
  {
    std::cout << "Could not open the response file\n";
    exit( EXIT_FAILURE );
  }

  // Read the whole file into a string
  std::stringstream ss;
  ss << ifs.rdbuf();

  // Split the file content
  std::string sep1("");
  std::string sep2(" \n\r");
  std::string sep3("\"");
  std::string sstr = ss.str();

  boost::escaped_list_separator<char> els( sep1, sep2, sep3 );
  boost::tokenizer< boost::escaped_list_separator<char> > tok( sstr, els );

  std::vector<std::string> args;
  std::copy( tok.begin(), tok.end(), std::back_inserter( args ) );

  // Parse the file and store the options
  store( po::command_line_parser( args ).options( all ).run(), vmap );    

  return vmap; 
}

//##############################################################################
// get_input().
//##############################################################################

ice_data_t
get_input( int argc, char **argv )
{

  ice_data_t ice_data;
  try
  {
    po::variables_map vmap;
    std::string s_nuc_xpath = "", s_reac_xpath = "", s_zone_xpath = "";

    po::options_description help( "\nHelp Options" );
    help.add_options()
    // Option to print help message for usage statement
    ( "help", "print out usage statement and exit\n" )

    // Option to print example usage
    ( "example", "print out example usage and exit\n" )

    // Option to specify response file
    ( "response-file", po::value<std::string>(), 
      "can be specified with '@name', too\n" )
  
    // Option to print help message for specific options
    ( "program_options", po::value<std::string>(), 
      "print out list of program options (help, general, output,"
      " yields, remnants, star_pop, graph, user, or all)"
      " and exit" );

    po::options_description general( "\nGeneral Options" );
    general.add_options()

    // Option for number of x zones
    (  S_N_X_ZONES, po::value<int>()->default_value( 10 ),
      "Number of x zones\n" )
    
    // Option for number of y zones
    (  S_N_Y_ZONES, po::value<int>()->default_value( 1 ),
      "Number of y zones\n" )
    
    // Option for number of z zones
    (  S_N_Z_ZONES, po::value<int>()->default_value( 1 ),
      "Number of z zones\n" )
    
    // Option for value of initial time
    (  nnt::s_TIME, po::value<double>()->default_value( 0., "0." ),
      "Initial time (years)\n" )
    
    // Option for value of initial time step
    (  nnt::s_DTIME, po::value<double>()->default_value( 1.e5, "1.e5" ),
      "Initial time step (years)\n" )
    
    // Option for value of initial time step
    (  S_DTMAX, po::value<double>()->default_value( 1.e5, "1.e5" ),
      "Maximum time step (years)\n" )
    
    // Option for value of duration of calculation
    (  nnt::s_TEND, po::value<double>()->default_value( 1.e9, "1.e9" ),
      "Duration of calculation (years)\n" )
    
    // Option for value of total mass
    (  S_TOTAL_MASS, po::value<double>()->default_value( 1.e8, "1.e8" ),
      "Total mass (solar masses)\n" )
    
    // Option for random number seed
    (  S_SEED, po::value<unsigned int>(),
      "Random number seed (optional)\n" )
    
    // Option for value of small abundances threshold
    (  S_SMALL_ABUND,
       po::value<double>()->default_value( 1.e-25, "1.e-25" ),
      "Small abundances threshold\n" )
    
    // Option for value of small rates threshold
    (  S_SMALL_RATES,
       po::value<double>()->default_value( 1.e-25, "1.e-25" ),
      "Small rates threshold\n" )
    
    // Option for value of max iterations
    (  S_ITMAX,
       po::value<int>()->default_value( 100 ),
      "Maximum number of iterations in exponential solver\n" )
    
    // Option for value of relative tolerance
    (  S_REL_TOL,
       po::value<double>()->default_value( 1.e-8, "1.e-8" ),
      "Relative tolerance for solutions\n" )
    
    // Option for value of relative tolerance
    (  S_DEBUG,
      "Print out debugging information about solutions (default: not set)\n" )
    
    // Option for value of star-formation timescale in years
    (  S_STAR_FORM, po::value<double>()->default_value( 1.e3, "1.e3" ), 
      "star-formation timescale (years)\n" )
    
    // Option for value of star-formation timescale in years
    (  S_SCHMIDT_EXPONENT, po::value<double>()->default_value( 1., "1." ), 
      "Schmidt exponent in star formation law\n" )
    
    // Option for value of cloud-mixing timescale in years
    (  S_CLOUD_MIX, po::value<double>()->default_value( 1.e6, "1.e6" ),
      "cloud-mixing timescale (years)\n" )
   
    // Option for value of XPath to select nuclei
    (
      S_NUC_XPATH,
      po::value<std::vector<std::string> >()->multitoken()->composing(), 
      "XPath to select nuclei (default: all nuclides)\n"
    )
    
    // Option for value of XPath to select reactions
    (
      S_REAC_XPATH,
      po::value<std::vector<std::string> >()->multitoken()->composing(),
      "XPath to select reactions (default: all reactions)\n"
    )
    
    // Option for value of XPath to select zones
    (
      S_ZONE_XPATH,
      po::value<std::vector<std::string> >()->multitoken()->composing(),
      "XPath to select zones (default: all zones)"
    )
    
    // Option for frequency of zone history saving
    ( S_HISTORY_STEPS, po::value<size_t>()->default_value( 100 ),
      "History steps." );

    po::options_description output( "\nOutput Options" );
    output.add_options()

    // Option for frequency of output
    ( nnt::s_STEPS, po::value<size_t>()->default_value( 50 ),
      "Frequency of output to main hdf5 file." )

    // Option for name of star output
    ( S_STARS_HDF5, po::value< std::vector<std::string > >(),
      "Hdf5 file for star output:lower mass:upper mass (default: not set)" )

    ;
    
    // Read in star_pop options
    
    po::options_description star_pop( "\nStar Population Options" );
    get_star_pop_descriptions( star_pop );

    // Read in graph options
    
    po::options_description graph( "\nGraph Options" );
    graph.add_options()

    // Option for name of output graph
    ( S_GRAPH_FILE, po::value<std::string>(),
      "Name of dot file for output graph" )
    
    // Option for time step to output graph
    ( S_GRAPH_STEP, po::value<size_t>()->default_value( 0 ),
      "Time step for output graph" );
    
    // Read in yield options
    
    po::options_description yields( "\nYield options");
    get_yields_descriptions( yields );
    
    // Read in remnants options
    
    po::options_description remnants( "\nRemnants options");
    get_remnants_descriptions( remnants );
    
    // Read in user-defined options
    
    po::options_description user("\nUser-defined options");
    get_user_defined_descriptions( user );
    
    // Declare an options description instance which will include all of the 
    // options
    
    po::options_description all( "\nAll Allowed Options" );
    all.add( help ).add( general ).add( output ).add( yields ).add( remnants).
        add( star_pop ).add( graph ).add( user );

    store( 
      po::command_line_parser( argc, argv ).
      options( all ).
      extra_parser( at_option_parser ).
      run(), 
      vmap 
    );

    if( argc == 1 || vmap.count( "help" ) == 1 )
    {
      std::cout << std::endl << "Usage: " << argv[0]
                << " net_xml abund_xml yields_xml output_hdf5 [options]"
                << std::endl; 
      std::cout << "\n  net_xml = input network xml file" << std::endl;
      std::cout << "\n  abund_xml = input initial abundances xml file"
                << std::endl;
      std::cout << "\n  yields_xml = input yields xml file" << std::endl;
      std::cout << "\n  output_hdf5 = output hdf5 file" << std::endl;
      std::cout << help << std::endl;
      exit( EXIT_FAILURE );
    }

    if( vmap.count( "example" ) == 1 )
    {
      std::cout << std::endl << argv[0]
                             << " ../data/ice_net.xml ../data/init_abunds.xml"
                             << " ../data/yields.xml @ice.rsp"
                             << std::endl << std::endl;
      exit( EXIT_FAILURE );
    }
                              
    if( vmap.count( "program_options" ) )
    {
      program_options(
        vmap,
        help,
        general,
        output,
        yields,
        remnants,
        star_pop,
        graph,
        user,
        all
      );
    }
      
    if( vmap.count( "response-file" ) ) 
      vmap = response_file( vmap, all );

    //==========================================================================
    // Get output data.
    //==========================================================================

    ice_data[S_HDF5] = std::string( argv[4] );

    ice_data[nnt::s_STEPS] = vmap[nnt::s_STEPS].as<size_t>();

    if( vmap.count(S_STARS_HDF5) )
    {
      std::vector<star_tup_t> v_star_tup;
      BOOST_FOREACH(
        std::string s,
        vmap[S_STARS_HDF5].as<std::vector<std::string> >()
      )
      {
        boost::char_separator<char> sep(":");
        boost::tokenizer<boost::char_separator<char> > tok( s, sep );

        std::vector<std::string> args;
        std::copy( tok.begin(), tok.end(), std::back_inserter( args ) );

        if( args.size() != 3 )
        {
          std::cerr << s << " is incorrect input for star data file.\n";
          exit( EXIT_FAILURE );
        }

        try
        {
          v_star_tup.push_back(
            boost::make_tuple(
              args[0],
              boost::lexical_cast<double>( args[1] ),
              boost::lexical_cast<double>( args[2] )
            )
          );
        }
        catch( const boost::bad_lexical_cast &e )
        {
          std::cerr << e.what() << std::endl;
          std::cerr << args[0] << "," << args[1] << "," << args[2]
                    << " are not valid input." << std::endl;
          exit( EXIT_FAILURE );
        }

      }
        
      ice_data[S_STARS_HDF5] = v_star_tup;
    }
    
    //==========================================================================
    // Get general data.
    //==========================================================================

    ice_data[S_N_X_ZONES] = vmap[S_N_X_ZONES].as<int>();
    ice_data[S_N_Y_ZONES] = vmap[S_N_Y_ZONES].as<int>();
    ice_data[S_N_Z_ZONES] = vmap[S_N_Z_ZONES].as<int>();

    ice_data[S_TOTAL_MASS] = vmap[S_TOTAL_MASS].as<double>();

    ice_data[nnt::s_TIME] = vmap[nnt::s_TIME].as<double>() * D_YEAR;
    ice_data[nnt::s_DTIME] = vmap[nnt::s_DTIME].as<double>() * D_YEAR;
    ice_data[nnt::s_TEND] = vmap[nnt::s_TEND].as<double>() * D_YEAR;
    ice_data[S_DTMAX] = vmap[S_DTMAX].as<double>() * D_YEAR;

    ice_data[S_SMALL_ABUND] = vmap[S_SMALL_ABUND].as<double>();
    ice_data[S_SMALL_RATES] = vmap[S_SMALL_RATES].as<double>();
    ice_data[S_ITMAX] = vmap[S_ITMAX].as<int>();
    ice_data[S_REL_TOL] = vmap[S_REL_TOL].as<double>();

    if( vmap.count( S_DEBUG ) ) {ice_data[S_DEBUG] = "yes";} 

    ice_data[S_NUCNET] = Libnucnet__new();

    ice_data[S_STAR_FORM] = vmap[S_STAR_FORM].as<double>() * D_YEAR;
    ice_data[S_SCHMIDT_EXPONENT] = vmap[S_SCHMIDT_EXPONENT].as<double>();
    ice_data[S_CLOUD_MIX] = vmap[S_CLOUD_MIX].as<double>() * D_YEAR;

    if( vmap.count( S_NUC_XPATH ) )
    {
      BOOST_FOREACH(
        std::string s,
        vmap[S_NUC_XPATH].as<std::vector<std::string> >()
      )
      {
        s_nuc_xpath += s + " ";
      }
    }

    if( vmap.count( S_REAC_XPATH ) )
    {
      BOOST_FOREACH(
        std::string s,
        vmap[S_REAC_XPATH].as<std::vector<std::string> >()
      )
      {
        s_reac_xpath += s + " ";
      }
    }

    Libnucnet__Net__updateFromXml(
      Libnucnet__getNet(
        boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] )
      ),  
      argv[1],
      s_nuc_xpath.c_str(),
      s_reac_xpath.c_str()
    );

    ice_data[S_STARS_NUCNET] =
      nnt::create_network_copy(
        boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] )
      );

    ice_data[S_ZONE_HISTORY_NUCNET] =
      nnt::create_network_copy(
        boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] )
      );

    if( vmap.count( S_ZONE_XPATH ) )
    {
      BOOST_FOREACH(
        std::string s,
        vmap[S_ZONE_XPATH].as<std::vector<std::string> >()
      )
      {
        s_zone_xpath += s + " ";
      }
    }

    Libnucnet__assignZoneDataFromXml(  
      boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] ),
      argv[2],
      s_zone_xpath.c_str()
    );

    ice_data[S_HISTORY_STEPS] = vmap[S_HISTORY_STEPS].as<size_t>();

    //==========================================================================
    // Get stellar population data.
    //==========================================================================

    set_star_pop_data( ice_data, vmap );

    //==========================================================================
    // Set yields.
    //==========================================================================

    set_yield_data( argv[3], ice_data, vmap );

    //==========================================================================
    // Set remnants.
    //==========================================================================

    set_remnants_data( ice_data, vmap );

    //==========================================================================
    // Get graph data.
    //==========================================================================

    if( vmap.count( S_GRAPH_FILE ) )
    {
      ice_data[S_GRAPH_FILE] = vmap[S_GRAPH_FILE].as<std::string>();
    }
    
    ice_data[S_GRAPH_STEP] = vmap[S_GRAPH_STEP].as<size_t>();

    //==========================================================================
    // Initialize variables and classes.
    //==========================================================================

    ice_data[S_ID] = static_cast<size_t>( 0 );

    ice_data[S_POP_HIST] =
      boost::shared_ptr<pop_hist>( new pop_hist( ice_data ) );

    ice_data[S_STARS_HEAP] = boost::shared_ptr<heap_t>( new heap_t );

    ice_data[S_ZONE_ID_MAP] =
      boost::shared_ptr<zone_id_map_t>( new zone_id_map_t );

    set_history_zones( ice_data );

    ice_data[S_HISTORY_DTIME] = 0.;

    boost::shared_ptr<mass_map_t> p_mass_map =
      boost::shared_ptr<mass_map_t>( new mass_map_t );

    (*p_mass_map)[S_ACTIVE_STAR] = 0.;
    (*p_mass_map)[S_INACTIVE_STAR] = 0.;
    (*p_mass_map)[S_WHITE_DWARF] = 0.;
    (*p_mass_map)[S_NEUTRON_STAR] = 0.;
    (*p_mass_map)[S_BLACK_HOLE] = 0.;

    ice_data[S_MASS_MAP] = p_mass_map;

    //==========================================================================
    // User data.
    //==========================================================================

    set_user_defined_options( vmap, ice_data );
   
    return ice_data;
  }
  catch( std::exception& e )
  {
    std::cerr << "Error: " << e.what() << "\n";
    exit( EXIT_FAILURE );
  }
  catch(...)
  {
    std::cerr << "Exception of unknown type!\n";
    exit( EXIT_FAILURE );
  }
}

//##############################################################################
// get_zone_masses().
//##############################################################################

std::vector<double>
get_zone_masses(
  ice_data_t& ice_data
)
{

  std::vector<double> masses;

  BOOST_FOREACH(
    nnt::Zone zone,
    nnt::make_zone_list(
      boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] )
    )
  )
  {
    masses.push_back( zone.getProperty<double>( nnt::s_ZONE_MASS ) );
  }

  return masses;

}

//##############################################################################
// set_star_properties().
//##############################################################################

void
set_star_properties(
  nnt::Zone& zone,
  const Star& star
)
{

  zone.updateProperty( S_METALLICITY, star.getMetallicity() );

  zone.updateProperty(
    S_FORMATION_TIME,
    star.getFormationTime()
  ); 

  zone.updateProperty(
    S_END_TIME,
    star.getEndTime()
  ); 

  zone.updateProperty(
    S_MASS,
    star.getMass()
  ); 

  zone.updateProperty(
    S_MY_X,
    star.getX()
  ); 

  zone.updateProperty(
    S_MY_Y,
    star.getY()
  ); 

  zone.updateProperty(
    S_MY_Z,
    star.getZ()
  ); 

  zone.updateProperty(
    S_MY_REAL_X,
    boost::lexical_cast<double>( star.getX() ) + star.getXFactor() 
  );

  zone.updateProperty(
    S_MY_REAL_Y,
    boost::lexical_cast<double>( star.getY() ) + star.getYFactor() 
  );

  zone.updateProperty(
    S_MY_REAL_Z,
    boost::lexical_cast<double>( star.getZ() ) + star.getZFactor() 
  );

  zone.updateProperty(
    S_NUMBER,
    star.getNumber()
  );

  zone.updateProperty(
    S_ID,
    star.getID()
  );

}


//##############################################################################
// insert_new_star_in_zone().
//##############################################################################

void
insert_new_star_in_zone(
  nnt::Zone& cloud_zone,
  Star& star
)
{

  star.setMetallicity(
    cloud_zone.getProperty<double>( S_METALLICITY )
  );

  double d_new_cloud_mass =
    cloud_zone.getProperty<double>( nnt::s_ZONE_MASS )
    -
    star.getNumber() * star.getMass();

  if( d_new_cloud_mass < 0 )
  {
    std::cerr << "Star mass greater than cloud mass." << std::endl;
    exit( EXIT_FAILURE );
  }

  cloud_zone.updateProperty(
    nnt::s_ZONE_MASS,
    d_new_cloud_mass
  );

}

//##############################################################################
// set_star().
//##############################################################################

void
add_star_to_vector(
  ice_data_t& ice_data,
  gsl_vector * p_abunds,
  Star star,
  std::vector<nnt::Zone>& v_stars
)
{

  nnt::Zone zone;

  if( ice_data.find( S_STARS_HDF5 ) != ice_data.end() )
  { 

    std::vector<star_tup_t> v_star_tup =
      boost::any_cast<std::vector<star_tup_t> >( ice_data[S_STARS_HDF5] );

    for( size_t i = 0; i < v_star_tup.size(); i++ )
    {

      if(
        star.getMass() >= v_star_tup[i].get<1>() &&
        star.getMass() <= v_star_tup[i].get<2>()
      )
      {

        zone.setNucnetZone(
          Libnucnet__Zone__new(
            Libnucnet__getNet(
              boost::any_cast<Libnucnet *>( ice_data[S_STARS_NUCNET] )
            ),
            "0",
            "0",
            "0"
          )
        );

        set_star_properties( zone, star );

        Libnucnet__Zone__updateAbundances(
          zone.getNucnetZone(),
          p_abunds
        );

        zone.updateProperty( S_STAR_FILE_ID, i );

        v_stars.push_back( zone );

      }

    }

  }

}

//##############################################################################
// write_star_to_hdf5().
//##############################################################################

void
write_star_to_hdf5( ice_data_t& ice_data, nnt::Zone& zone )
{

  Libnucnet * p_stars =
        boost::any_cast<Libnucnet *>( ice_data[S_STARS_NUCNET] );

  Libnucnet__addZone( p_stars, zone.getNucnetZone() );

  boost::format fmt( "%015d" );
  fmt % zone.getProperty<size_t>( S_ID );

  std::vector<star_tup_t> v_star_tup =
    boost::any_cast<std::vector<star_tup_t> >( ice_data[S_STARS_HDF5] );

  user::hdf5::append_zones(
    v_star_tup[zone.getProperty<size_t>( S_STAR_FILE_ID )].get<0>().c_str(),
    p_stars,
    ( "Star " + fmt.str() ).c_str()
  );

  Libnucnet__removeZone( p_stars, zone.getNucnetZone() );

}

//##############################################################################
// compute_number_in_mean().
//##############################################################################

int
compute_number_in_mean(
  boost::mt19937& gen,
  double mean
)
{

  boost::random::poisson_distribution<> pdist( mean );

  boost::variate_generator<
    boost::mt19937&,
    boost::random::poisson_distribution<int>
  > pvt( gen, pdist );

  return pvt();

}

//##############################################################################
// compute_mass().
//##############################################################################

double
compute_mass( boost::mt19937& gen, imf_deviate& imf_dev )
{

  boost::random::uniform_01<> mdist;

  boost::variate_generator<
    boost::mt19937&, boost::random::uniform_01<>
  > mvt( gen, mdist );

  return imf_dev.getMass( mvt() );

}

//##############################################################################
// compute_formation_time().
//##############################################################################

double
compute_formation_time(
  boost::mt19937& gen,
  double d_t1,
  double d_t2
)
{

  boost::random::uniform_real_distribution<> tdist( d_t1, d_t2 );

  boost::variate_generator<
    boost::mt19937&, boost::random::uniform_real_distribution<>
  > rvt( gen, tdist );

  return rvt();

}

//##############################################################################
// compute_total_masses().
//##############################################################################

std::map<std::string,double>
compute_total_masses( ice_data_t& ice_data )
{

  std::map<std::string,double> result;

  mass_map_t my_mass_map =
    *(boost::any_cast<boost::shared_ptr<mass_map_t> >(
      ice_data[S_MASS_MAP]
     )
    );

  result[S_HALO] = 0;
  result[S_GAS] = 0;

  BOOST_FOREACH( mass_map_t::value_type& m, my_mass_map )
  {
    result[m.first] = m.second;
  }

  heap_t my_heap = 
    *(boost::any_cast<boost::shared_ptr<heap_t> >(
      ice_data[S_STARS_HEAP]
     )
    );

  BOOST_FOREACH(
    nnt::Zone zone,
    nnt::make_zone_list(
      boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] )
    )
  )
  {
    if(
      std::string( Libnucnet__Zone__getLabel( zone.getNucnetZone(), 1 ) )
      ==
      S_HALO
    )
    {
      result[S_HALO] += zone.getProperty<double>( nnt::s_ZONE_MASS );
    }
    else
    {
      result[S_GAS] += zone.getProperty<double>( nnt::s_ZONE_MASS );
    }
  }

  double total = 0;

  BOOST_FOREACH( mass_map_t::value_type& m, result )
  {
    total += m.second;
  }

  result[S_TOTAL] = total;

  return result;

}
  
//##############################################################################
// update_zone_metallicities().
//##############################################################################

void
update_zone_metallicities( Libnucnet * p_nucnet )
{

  std::vector<nnt::Zone> zones;

  BOOST_FOREACH( nnt::Zone zone, nnt::make_zone_list( p_nucnet ) )
  {
    zones.push_back( zone );
  }

#ifndef NO_OPENMP
  #pragma omp parallel for schedule( dynamic, 1 )
#endif
  for( size_t i = 0; i < zones.size(); i++ )
  {
    zones[i].updateProperty(
      S_METALLICITY,
      user::compute_cluster_abundance_moment(
        zones[i],
        "[z > 2]",
        "a",
        1
      )
    );
  }

}

//##############################################################################
// update_stars().
//##############################################################################

void
update_stars(
  boost::mt19937& gen,
  ice_data_t& ice_data,
  double d_t1,
  double d_t2
)
{

  typedef
    ac::accumulator_set<
      double,
      ac::stats<
        ac::tag::mean(ac::immediate),
        ac::tag::sum,
        ac::tag::moment<2>
      >
    > accum_t;

  typedef std::map<double, accum_t> star_map_t;

  StarSystemGenerator ss_gen;

  zone_id_map_t zone_id_map =
    *boost::any_cast<boost::shared_ptr<zone_id_map_t> >(
      ice_data[S_ZONE_ID_MAP]
    );

  std::vector<nnt::Zone> star_forming_zones;

  std::vector<pop_hist> v_pop;

  std::vector<size_t> n_stars, zone_id;

  std::vector<double> mass_bins =
    boost::assign::list_of(0.001)(0.4)(0.5)(0.6)(0.7)(0.8);

  Libnucnet * p_my_nucnet = boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] );

  update_zone_metallicities( p_my_nucnet );

  BOOST_FOREACH( nnt::Zone zone, get_star_forming_zones( p_my_nucnet ) )
  {

    star_forming_zones.push_back( zone );

    v_pop.push_back( pop_hist( ice_data ) );

    double mean =
      ( d_t2 - d_t1 ) *
      ( 1. / boost::any_cast<double>( ice_data[S_STAR_FORM] ) ) *
      pow(
        zone.getProperty<double>( nnt::s_ZONE_MASS ) / 1.e6,
        boost::any_cast<double>( ice_data[S_SCHMIDT_EXPONENT] )
      );

    zone_id_map_t::iterator it = zone_id_map.find( zone );

    if( it == zone_id_map.end() )
    {
      std::cerr << "History zone not found." << std::endl;
      exit( EXIT_FAILURE );
    }
    else
    {
      zone_id.push_back( it->second );
    }

    n_stars.push_back( compute_number_in_mean( gen, mean ) );

  }

  std::vector<size_t> v_id( n_stars.size() );

  std::vector<heap_t> v_heap( n_stars.size() );

  std::vector<double>
    v_cum_active_star_mass( n_stars.size() ),
    v_cum_inactive_star_mass( n_stars.size() );

  std::vector<std::vector<nnt::Zone> > v_stars( n_stars.size() );

  v_id[0] = boost::any_cast<size_t>( ice_data[S_ID] );

  for( size_t i = 1; i < n_stars.size(); i++ )
  {
    v_id[i] = v_id[i-1] + n_stars[i-1];
  }

#ifndef NO_OPENMP
  #pragma omp parallel for schedule( dynamic, 1 )
#endif
  for( size_t i = 0; i < n_stars.size(); i++ )
  {

    imf_deviate imf_dev( ice_data );

    boost::random::random_device dev;
    boost::mt19937 my_gen( dev );

    double d_available_mass =
      star_forming_zones[i].getProperty<double>( nnt::s_ZONE_MASS );
      
    star_map_t star_map;

    gsl_vector * p_abunds =
      Libnucnet__Zone__getAbundances( star_forming_zones[i].getNucnetZone() );

    size_t j = n_stars[i];

    while( j > 0 )
    {

      std::vector<double> v_star_masses;

      for( size_t k = 0; k < ss_gen( my_gen ); k++ )
      {
        v_star_masses.push_back( compute_mass( my_gen, imf_dev ) );
      }

      double d_system_mass =
        std::accumulate( v_star_masses.begin(), v_star_masses.end(), 0. );

      double d_max_mass =
        *std::max_element( v_star_masses.begin(), v_star_masses.end() );

      if( d_available_mass > d_system_mass && j >= v_star_masses.size() )
      {
        d_available_mass -= d_system_mass;

        j -= v_star_masses.size();

        BOOST_FOREACH( double mass, v_star_masses )
        {

          if( ice_data.find( S_POP_FILE ) != ice_data.end() )
          {
            v_pop[i].update( mass, 1 );
          }

        }

        if( d_max_mass > mass_bins[mass_bins.size() - 1] )
        {

          StarSystem star_system;

          boost::tuple<double,double,double> t =
            compute_3d_subgrid_location( my_gen );

          BOOST_FOREACH( double mass, v_star_masses )
          {

            accum_t my_acc;

            my_acc( mass );

            stats
              st(
                ac::count( my_acc ),
                ac::mean( my_acc ),
                ac::moment<2>( my_acc )
              );

            Star
              star(
                st,
                d_t1,
                v_id[i]++,
                zone_id[i],
                get_zone_labels_as_integer_tuple( star_forming_zones[i] ),
                t
              );

            insert_new_star_in_zone(
              star_forming_zones[i],
              star
            );

            star_system.addStar( star );

            if(
                star.getEndTime() <=
                boost::any_cast<double>( ice_data[nnt::s_TEND] )
            )
            {
              v_cum_active_star_mass[i] += star.getNumber() * star.getMass();
            }
            else
            {
              v_cum_inactive_star_mass[i] += star.getNumber() * star.getMass();
            }

          }

          if(
            star_system.getTopStar().getEndTime() <=
            boost::any_cast<double>( ice_data[nnt::s_TEND] )
          )
          {
            v_heap[i].push( star_system );
          }
          else
          {
            while( star_system.getNumberOfStars() > 0 )
            {
              add_star_to_vector(
                ice_data,
                p_abunds,
                star_system.getTopStar(),
                v_stars[i]
              );
              star_system.popTopStar();
            }
          }
        }
        else
        {
          BOOST_FOREACH( double mass, v_star_masses )
          {
            size_t i_mass =
              std::lower_bound(
                mass_bins.begin(), mass_bins.end(), mass
            )
            -
            mass_bins.begin();

            double f_mass = mass_bins[i_mass - 1];
            star_map_t::iterator it = star_map.find( f_mass );
            if( it != star_map.end() )
            {
              it->second( mass );
            }
            else
            {
              star_map[f_mass]( mass );
            }
          }
        }
      }
    }

    for(
    star_map_t::iterator it = star_map.begin();
    it != star_map.end();
    it++
    )
    {

      StarSystem star_system;

      stats
        st(
          ac::count( it->second ),
          ac::mean( it->second ),
          ac::moment<2>( it->second )
        );

      Star
        star(
          st,
          d_t1,
          v_id[i]++,
          zone_id[i],
          get_zone_labels_as_integer_tuple( star_forming_zones[i] ),
          compute_3d_subgrid_location( my_gen )
        );

      if(
        star.getEndTime() <= boost::any_cast<double>( ice_data[nnt::s_TEND] )
      )
      {
        star_system.addStar( star );
        v_heap[i].push( star_system );
      }
      else
      {
        add_star_to_vector(
          ice_data,
          p_abunds,
          star,
          v_stars[i]
        );
      }

      insert_new_star_in_zone(
        star_forming_zones[i],
        star
      );

      if(
          star.getEndTime() <= boost::any_cast<double>( ice_data[nnt::s_TEND] )
      )
      {
        v_cum_active_star_mass[i] += star.getNumber() * star.getMass();
      }
      else
      {
        v_cum_inactive_star_mass[i] += star.getNumber() * star.getMass();
      }

    }

    gsl_vector_free( p_abunds );

  }

  for( size_t i = 0; i < v_heap.size(); i++ )
  {

    boost::any_cast<boost::shared_ptr<heap_t> >(
      ice_data[S_STARS_HEAP]
    )->merge( v_heap[i] );

    if( ice_data.find( S_POP_FILE ) != ice_data.end() )
    {
      *(boost::any_cast<
          boost::shared_ptr<pop_hist>
        >( ice_data[S_POP_HIST] ) ) += v_pop[i];
    }

    if( ice_data.find( S_STARS_HDF5 ) != ice_data.end() )
    {
      BOOST_FOREACH( nnt::Zone star_zone, v_stars[i] )
      {
        write_star_to_hdf5( ice_data, star_zone );
      }
    }

    boost::shared_ptr<mass_map_t> p_mass_map =
      boost::any_cast<
        boost::shared_ptr<mass_map_t>
      >( ice_data[S_MASS_MAP] );

    (*p_mass_map)[S_ACTIVE_STAR] += v_cum_active_star_mass[i];
    (*p_mass_map)[S_INACTIVE_STAR] += v_cum_inactive_star_mass[i];

  }

  ice_data[S_ID] = v_id[v_id.size()-1];

}

//##############################################################################
// Create multi-d zones.
//##############################################################################

void
create_multi_d_zones(
  boost::tuple<int,int,int>& n_zones,
  ice_data_t& ice_data
)
{

  Libnucnet * p_nucnet =
    boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] );

  Libnucnet__Zone * p_initial_zone =
    Libnucnet__getZoneByLabels( p_nucnet, "0", "0", "0" );

  gsl_vector * p_abunds =
    Libnucnet__Zone__getAbundances( p_initial_zone );

  Libnucnet__removeZone( p_nucnet, p_initial_zone );

  for( int i = 0; i < n_zones.get<0>(); i++ )
  {
    for( int j = 0; j < n_zones.get<1>(); j++ )
    {
      for( int k = 0; k < n_zones.get<2>(); k++ )
      {
        Libnucnet__Zone * p_zone =
          Libnucnet__Zone__new(
            Libnucnet__getNet( p_nucnet ),
            boost::lexical_cast<std::string>( i ).c_str(),
            boost::lexical_cast<std::string>( j ).c_str(),
            boost::lexical_cast<std::string>( k ).c_str()
          );

        Libnucnet__Zone__updateAbundances( p_zone, p_abunds );
        Libnucnet__addZone( p_nucnet, p_zone );
        
      }
    }
  }

  gsl_vector_free( p_abunds );

}

//##############################################################################
// remove_star_from_population_hist().
//##############################################################################

void
remove_star_from_population_hist(
  ice_data_t& ice_data,
  Star& s
)
{

  if( s.getStatus() == S_STAR )
  {
    boost::any_cast<
      boost::shared_ptr<pop_hist>
    >( ice_data[S_POP_HIST] )->update(
      s.getMass(),
      -static_cast<int>( s.getNumber() )
    );
  }

}

//##############################################################################
// add_stellar_debris_and_update_system().
//##############################################################################

void
add_stellar_debris_and_update_system(
  ice_data_t& ice_data,
  nnt::Zone& zone,
  StarSystem& star_system
)
{

  Star star = star_system.getTopStar();

  gsl_vector * p_new;

  double ejected_mass, remnant_mass;

  //============================================================================
  // Get injecta zone masses.
  //============================================================================

  gsl_vector * p_old =
    Libnucnet__Zone__getMassFractions( zone.getNucnetZone() );

  gsl_vector_scale(
    p_old,
    zone.getProperty<double>( nnt::s_ZONE_MASS )
  );

  //============================================================================
  // Get yields.
  //============================================================================

  boost::tie( p_new, ejected_mass, remnant_mass ) =
    get_yield_data( ice_data, star_system );

  double d_yield_mass = star.getNumber() * ejected_mass;

  gsl_vector_scale(
    p_new,
    d_yield_mass
  );

  //============================================================================
  // Add stellar debris to zone.
  //============================================================================

  gsl_vector_add( p_old, p_new );

  double d_zone_mass =
    zone.getProperty<double>( nnt::s_ZONE_MASS )
    +
    d_yield_mass;

  gsl_vector_scale(
    p_old,
    1. / d_zone_mass
  );

  Libnucnet__Zone__updateMassFractions( zone.getNucnetZone(), p_old );

  // Add star's mass to zone.

  zone.updateProperty(
    nnt::s_ZONE_MASS,
    d_zone_mass
  ); 

  //============================================================================
  // Update.
  //============================================================================

  update_stellar_system( ice_data, star_system, remnant_mass );

  gsl_vector_free( p_old );
  gsl_vector_free( p_new );

}

//##############################################################################
// subtract_total_masses().
//##############################################################################

void
subtract_total_masses(
  ice_data_t& ice_data,
  boost::tuple<nnt::Zone, heap_t, mass_map_t>& t,
  StarSystem& ss
)
{

  BOOST_FOREACH( Star s, ss.getStarHeap() )
  {
    if( s.getStatus() == S_STAR )
    {
      if( s.getEndTime() <= boost::any_cast<double>( ice_data[nnt::s_TEND] ) )
      {
        (t.get<2>())[S_ACTIVE_STAR] -= s.getNumber() * s.getMass();
      }
      else
      { 
        (t.get<2>())[S_INACTIVE_STAR] -= s.getNumber() * s.getMass();
      }
    }
    else
    {
        (t.get<2>())[s.getStatus()] -= s.getNumber() * s.getMass();
    }
  }

}

//##############################################################################
// add_total_masses().
//##############################################################################

void
add_total_masses(
  ice_data_t& ice_data,
  boost::tuple<nnt::Zone, heap_t, mass_map_t>& t,
  StarSystem& ss
)
{

  BOOST_FOREACH( Star s, ss.getStarHeap() )
  {
    if( s.getStatus() == S_STAR )
    {
      if( s.getEndTime() <= boost::any_cast<double>( ice_data[nnt::s_TEND] ) )
      {
        (t.get<2>())[S_ACTIVE_STAR] += s.getNumber() * s.getMass();
      }
      else
      { 
        (t.get<2>())[S_INACTIVE_STAR] += s.getNumber() * s.getMass();
      }
    }
    else
    {
        (t.get<2>())[s.getStatus()] += s.getNumber() * s.getMass();
    }
  }

}

//##############################################################################
// add_ejecta().
//##############################################################################

void
add_ejecta( ice_data_t& ice_data, double t )
{

  typedef std::map<nnt::Zone, heap_t > my_map_t;

  my_map_t my_map;
  mass_map_t mass_map;
  std::vector<boost::tuple<nnt::Zone, heap_t, mass_map_t> > v;

  boost::shared_ptr<heap_t> p_stars =
    boost::any_cast<boost::shared_ptr<heap_t> >( ice_data[S_STARS_HEAP] );

  boost::shared_ptr<mass_map_t> p_mass_map =
    boost::any_cast<boost::shared_ptr<mass_map_t> >(
      ice_data[S_MASS_MAP]
    );

  BOOST_FOREACH( mass_map_t::value_type& m, *p_mass_map )
  {
    mass_map[m.first] = 0;
  }

  if( ice_data.find( S_POP_FILE ) != ice_data.end() )
  {
    for(
      heap_t::ordered_iterator it = p_stars->ordered_begin();
      it != p_stars->ordered_end();
      it++
    )
    {
      Star s = it->getTopStar();
      if( t > s.getEndTime() )
      {
        remove_star_from_population_hist( ice_data, s );
      }
      else
      {
        break;
      }
    }
  }
    
  while(
    p_stars->size() != 0 &&
    t > (p_stars->top()).getTopStar().getEndTime()
  )
  {
    StarSystem star_system = p_stars->top();
    p_stars->pop();
    nnt::Zone zone = get_injecta_zone( ice_data, star_system );
    my_map[zone].push( star_system );
  }

  BOOST_FOREACH( my_map_t::value_type& p, my_map )
  {
    v.push_back( boost::make_tuple( p.first, p.second, mass_map ) );
  }

#ifndef NO_OPENMP
  #pragma omp parallel for schedule( dynamic, 1 )
#endif
  for( size_t i = 0; i < v.size(); i++ )
  {
    while(
      !(v[i].get<1>()).empty() &&
      t > ((v[i].get<1>()).top()).getTopStar().getEndTime()
    )
    {
      StarSystem star_system = (v[i].get<1>()).top();
      (v[i].get<1>()).pop();
      subtract_total_masses( ice_data, v[i], star_system );
      add_stellar_debris_and_update_system(
        ice_data,
        v[i].get<0>(),
        star_system
      );
      add_total_masses( ice_data, v[i], star_system );
      update_stellar_system_heap( ice_data, v[i].get<1>(), star_system );
    }
  }

  for( size_t i = 0; i < v.size(); i++ )
  {
    p_stars->merge( v[i].get<1>() );
    BOOST_FOREACH( mass_map_t::value_type& m, v[i].get<2>() )
    {
      (*p_mass_map)[m.first] += m.second;
    }
  }

}

//##############################################################################
// compute_3d_subgrid_location().
//##############################################################################

boost::tuple<double,double,double>
compute_3d_subgrid_location(
  boost::mt19937& gen
)
{
  std::vector<double> vec;

  for( int i = 0; i < 3; i++ )
  {
    boost::random::uniform_01<> xdistFactor;

    boost::variate_generator<
      boost::mt19937&, boost::random::uniform_01<>
    > xvtFactor( gen, xdistFactor );

    vec.push_back( xvtFactor() );
  }

  return boost::make_tuple( vec[0], vec[1], vec[2] );
}

//##############################################################################
// update_common_zone_properties()
//##############################################################################

void
update_common_zone_properties(
  std::vector<nnt::Zone>& zones,
  ice_data_t& ice_data
)
{

  BOOST_FOREACH( nnt::Zone zone, zones )
  {
    zone.updateProperty( nnt::s_T9, 1.e-8 );
    zone.updateProperty( nnt::s_RHO, 1. );
    zone.updateProperty( nnt::s_MU_NUE_KT, "-inf" );
    zone.updateProperty(
      nnt::s_SMALL_RATES_THRESHOLD,
      boost::any_cast<double>( ice_data[S_SMALL_RATES] )
    );
    zone.updateProperty(
      nnt::s_TIME,
      boost::any_cast<double>( ice_data[nnt::s_TIME] )
    );
  }

}

//##############################################################################
// output_mass_data().
//##############################################################################

void
output_mass_data( ice_data_t& ice_data )
{

  if( ice_data.find( S_MASS_FILE ) != ice_data.end() )
  {

    std::map<std::string,double> masses = compute_total_masses( ice_data );

    std::ofstream my_file;

    my_file.open(
      boost::any_cast<std::string>( ice_data[S_MASS_FILE] ).c_str(),
      std::ios::app
    );

    my_file << boost::any_cast<double>( ice_data[nnt::s_TIME] ) << "  " <<
               masses[S_HALO] << "  " <<
               masses[S_GAS] << "  " <<
               masses[S_ACTIVE_STAR] << "  " <<
               masses[S_INACTIVE_STAR] << "  " <<
               masses[S_WHITE_DWARF] << "  " <<
               masses[S_NEUTRON_STAR] << "  " <<
               masses[S_BLACK_HOLE] << "  " <<
               masses[S_TOTAL] << std::endl;

    my_file.close();

  }

}

//##############################################################################
// clean_up().
//##############################################################################

void
clean_up( ice_data_t& ice_data )
{

  const std::vector<std::string> str =
    boost::assign::list_of(S_NUCNET)(S_STARS_NUCNET)(S_ZONE_HISTORY_NUCNET);

  BOOST_FOREACH( std::string s, str )
  {
    Libnucnet__free( boost::any_cast<Libnucnet *>( ice_data[s] ) );
  }

  yields_cleanup( ice_data );

}
