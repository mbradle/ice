////////////////////////////////////////////////////////////////////////////////
//  
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header for utilities types in multi-zone chemical evolution
//!        code.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef ICE_UTILITIES_TYPES_H
#define ICE_UTILITIES_TYPES_H

//##############################################################################
// Types.
//##############################################################################

namespace po = boost::program_options; 

namespace ac = boost::accumulators;

typedef std::map<std::string, boost::any> ice_data_t;

typedef std::map<nnt::Zone, size_t> zone_id_map_t;

typedef std::map<std::string, double> mass_map_t;

typedef boost::tuple<int,int,int> pos_t;

typedef boost::tuple<std::string, double, double> star_tup_t;

#endif  // ICE_UTILITIES_TYPES_H
