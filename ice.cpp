////////////////////////////////////////////////////////////////////////////////
//  
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to mix mass and species over in a chemical evolution context.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <boost/graph/graphviz.hpp>
#include <boost/graph/iteration_macros.hpp>

#include "ice_utilities.h"

//##############################################################################
// set_solver_params().
//##############################################################################

void
set_solver_parameters( WnSparseSolve__Phi * p_phi, ice_data_t& ice_data )
{

  //============================================================================
  // Set parameters for iterative solver.
  //============================================================================

  WnSparseSolve__Phi__updateTolerance(
    p_phi,
    boost::any_cast<double>( ice_data[S_REL_TOL] )
  );

  WnSparseSolve__Phi__updateMaximumIterations(
    p_phi,
    boost::any_cast<int>( ice_data[S_ITMAX] )
  );

  if( ice_data.find( S_DEBUG ) != ice_data.end() )
  {
    WnSparseSolve__Phi__setDebug( p_phi );
  }

}

//##############################################################################
// output_graph().
//##############################################################################

template<class Graph>
void
output_graph( Graph& g, ice_data_t& ice_data )
{

  if( ice_data.find( S_GRAPH_FILE ) != ice_data.end() )
  {
    if(
      boost::any_cast<size_t>( ice_data[S_GRAPH_STEP] ) ==
      boost::any_cast<size_t>( ice_data[S_STEP] )
    )
    {
      std::ofstream my_file(
        boost::any_cast<std::string>( ice_data[S_GRAPH_FILE] ).c_str()
      );
      boost::write_graphviz(
        my_file,
        g,
        user::multi_zone_vertex_writer( g ),
        user::multi_zone_edge_writer( g )
      );
      my_file.close();
    }
  }

}

//##############################################################################
// get_matrices().
//##############################################################################

std::vector<WnMatrix *>
get_matrices( std::vector<nnt::Zone>& zones, ice_data_t& ice_data )
{
  std::vector<WnMatrix *> mix_matrices;
  typedef user::zone_link_graph_t Graph;

  Graph g = user::make_zone_link_graph( zones );

  set_links( g, zones, ice_data );

  output_graph( g, ice_data );

  size_t i_species =
    Libnucnet__Nuc__getNumberOfSpecies(
      Libnucnet__Net__getNuc(
        Libnucnet__Zone__getNet( zones[0].getNucnetZone() )
      )
    );
  size_t i_offset = i_species + 1;
  size_t i_total_size = zones.size() * i_offset;

  for( size_t i = 0; i < zones.size(); i++ )
    mix_matrices.push_back( WnMatrix__new( i_total_size, i_total_size ) );

  nnt::species_list_t species_list =
    nnt::make_species_list(
      Libnucnet__Net__getNuc(
        Libnucnet__Zone__getNet( zones[0].getNucnetZone() )
      )
    );

  BGL_FORALL_VERTICES( v_from, g, Graph )
  {
    size_t i_from = v_from;

    BGL_FORALL_OUTEDGES( v_from, e, g, Graph )
    {
      Graph::vertex_descriptor v_to = boost::target( e, g );

      size_t i_to = v_to;

      WnMatrix__assignElement(
	mix_matrices[i_from],
	i_offset * i_from + 1,
	i_offset * i_from + 1,
	-g[e].getWeight()
      );

      WnMatrix__assignElement(
        mix_matrices[i_from],
	i_offset * i_to + 1,
	i_offset * i_from + 1,
	g[e].getWeight()
      );

      BOOST_FOREACH( nnt::Species species, species_list )
      {
	size_t i_species_offset =
	  Libnucnet__Species__getIndex( species.getNucnetSpecies() ) + 1;

	WnMatrix__assignElement(
	  mix_matrices[i_from],
	  ( i_offset * i_from ) + i_species_offset + 1,
	  ( i_offset * i_from ) + i_species_offset + 1,
	  -g[e].getWeight()
	);

	WnMatrix__assignElement(
	  mix_matrices[i_from],
	  ( i_offset * i_to ) + i_species_offset + 1,
	  ( i_offset * i_from ) + i_species_offset + 1,
	  g[e].getWeight()
	);
      }
    }
  }

  return mix_matrices;
}

//##############################################################################
// check_multi_zone_solutions().
//##############################################################################

int
check_multi_zone_solutions(
  std::vector<nnt::Zone>& zones,
  ice_data_t& ice_data
)
{

  std::vector<double> dxsum( zones.size() );
  std::vector<double> d_zone_mass( zones.size() );
  double d_zone_mass_min;

#ifndef NO_OPENMP
  #pragma omp parallel for schedule( dynamic, 1 )
#endif
    for( size_t i = 0; i < zones.size(); i++ )
    {
      d_zone_mass[i] =
        zones[i].getProperty<double>( nnt::s_ZONE_MASS );
/*
      dxsum[i] =
        1. - Libnucnet__Zone__computeAMoment( zones[i].getNucnetZone(), 1 );
*/
    }

  d_zone_mass_min =
    *(std::min_element( d_zone_mass.begin(), d_zone_mass.end() ) );

//  d_x_max = *(std::max_element( dxsum.begin(), dxsum.end() ) );

  if( d_zone_mass_min < 0 )
  {
    std::cout << "Min mass = " << "  " << d_zone_mass_min << std::endl;
    return 0;
  }
  else
    return 1;

}

//##############################################################################
// evolve_zones().
//##############################################################################

int
evolve_zones( 
  std::vector<nnt::Zone>& zones, 
  double dt,
  ice_data_t& ice_data 
)
{

  return
    user::exp_multi_zone(
      zones,
      boost::bind( get_matrices, boost::ref( zones ), boost::ref( ice_data ) ),
      boost::bind( set_solver_parameters, _1, boost::ref( ice_data ) ),
      boost::bind(
        check_multi_zone_solutions,
        boost::ref( zones ),
        boost::ref( ice_data )
      ),
      dt
    );
}

//##############################################################################
// main().
//##############################################################################

int
main( int argc, char **argv )
{
  ice_data_t ice_data;
  Libnucnet * p_my_nucnet;

  //============================================================================
  // Get input.
  //============================================================================
 
  ice_data = get_input( argc, argv );
  
  p_my_nucnet = boost::any_cast<Libnucnet *>( ice_data[S_NUCNET] );

  std::vector< std::pair<std::string,std::string> > my_views;
  std::vector<nnt::Zone> all_zones;

  double dt, dt_max = 0, t, t_next, tend;

  //============================================================================
  // Get grid.
  //============================================================================
  
  boost::tuple<int,int,int> n_zones =
    boost::make_tuple(
      boost::any_cast<int>( ice_data[S_N_X_ZONES] ),
      boost::any_cast<int>( ice_data[S_N_Y_ZONES] ),
      boost::any_cast<int>( ice_data[S_N_Z_ZONES] )
    );

  //============================================================================
  // Create multi-d zones.
  //============================================================================
  
  create_multi_d_zones( n_zones, ice_data );

  //============================================================================
  // Get all the zones.
  //============================================================================

  Libnucnet__setZoneCompareFunction(
    p_my_nucnet,
    (Libnucnet__Zone__compare_function) nnt::zone_compare_by_first_label
  );

  all_zones = user::get_vector_of_zones( p_my_nucnet );

  //============================================================================
  // Set zones.
  //============================================================================
  
  set_zones( all_zones, ice_data );

  //============================================================================
  // Set zone views.
  //============================================================================
  
  my_views.push_back( std::make_pair( "", "" ) );

  user::add_views_to_zones( all_zones, my_views );

  //============================================================================
  // Initial network limiting
  //============================================================================
  
  double d_min_abund = boost::any_cast<double>( ice_data[S_SMALL_ABUND] );

  user::limit_zone_networks( all_zones, d_min_abund );
  user::multi_zone_zero_small_abundances( all_zones, d_min_abund );

  //============================================================================
  // Normalize abundances.
  //============================================================================

  user::normalize_multi_zone_abundances( all_zones );

  //============================================================================
  // Create the output.
  //============================================================================
  
  user::hdf5::create_output(
    boost::any_cast<std::string>( ice_data[S_HDF5] ).c_str(),
    p_my_nucnet
  );

  if( ice_data.find( S_STARS_HDF5 ) != ice_data.end() )
  {
    BOOST_FOREACH(
      star_tup_t t, 
      boost::any_cast<std::vector<star_tup_t> >( ice_data[S_STARS_HDF5] )
    )
    {
      user::hdf5::create_output(
        t.get<0>().c_str(),
        boost::any_cast<Libnucnet *>( ice_data[S_STARS_NUCNET] )
      );
    }
  }

  //============================================================================
  // Set random number generator seed.
  //============================================================================
  
  boost::mt19937 gen;

  if( ice_data.find( S_SEED ) != ice_data.end() )
    gen.seed( boost::any_cast<unsigned int>( ice_data[S_SEED] ) );
  else
    gen.seed( static_cast<unsigned int>( std::time( 0 ) ) );

  //============================================================================
  // Output IMF data.
  //============================================================================
  
  output_imf_deviate( ice_data );

  //============================================================================
  // Evolve.
  //============================================================================
  
  dt_max = boost::any_cast<double>( ice_data[S_DTMAX] );
  t = boost::any_cast<double>( ice_data[nnt::s_TIME] );
  dt = boost::any_cast<double>( ice_data[nnt::s_DTIME] );
  tend = boost::any_cast<double>( ice_data[nnt::s_TEND] );

  size_t i_step = 0;

  while( t <= tend )
  {
    t += dt;
    ice_data[S_STEP] = i_step++;
    ice_data[nnt::s_TIME] = t;
    ice_data[nnt::s_DTIME] = dt;
    boost::shared_ptr<heap_t> p_stars =
      boost::any_cast<boost::shared_ptr<heap_t> >( ice_data[S_STARS_HEAP] );

    update_common_zone_properties( all_zones, ice_data );
    update_history_zones( ice_data );
    
    if( p_stars->size() != 0 )
      t_next = (p_stars->top()).getTopStar().getEndTime();
    else
      t_next = 1.e99;

    std::cout << "dt = " << dt << " time = " << t << " tend = " <<
      boost::any_cast<double>( ice_data[nnt::s_TEND] )
      << " tnext = " <<  t_next << std::endl;

    if( !evolve_zones( all_zones, dt, ice_data ) )
    {
      std::cerr << "Unable to find matrix solution." << std::endl;
      return EXIT_FAILURE;
    }

    dt *= 1.15;

    if( dt > dt_max ) dt = dt_max;

    user::limit_zone_networks( all_zones, d_min_abund );
    user::multi_zone_zero_small_abundances( all_zones, d_min_abund );

    if(
      i_step % boost::any_cast<size_t>( ice_data[nnt::s_STEPS] ) == 0 ||
      t >= tend
    )
    {
      user::hdf5::append_zones(
        boost::any_cast<std::string>( ice_data[S_HDF5] ).c_str(),
        p_my_nucnet
      );
      output_star_pop( ice_data );
      output_mass_data( ice_data );
    }

    add_ejecta( ice_data, t );

    update_stars( gen, ice_data, t, t + dt );

  }

  //============================================================================
  // Clean up and exit.
  //============================================================================

  clean_up( ice_data );

  return EXIT_SUCCESS;
}
