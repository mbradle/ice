#///////////////////////////////////////////////////////////////////////////////
#  Copyright (c) 2013 Clemson University.
# 
#  This file was originally written by Bradley S. Meyer.
# 
#  This is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
# 
#  This software is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this software; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#  USA
# 
#/////////////////////////////////////////////////////////////////////////////*/

#///////////////////////////////////////////////////////////////////////////////
#//!
#//! \file Makefile
#//! \brief A makefile to generate codes for the multi-zone project.
#//!
#///////////////////////////////////////////////////////////////////////////////

#///////////////////////////////////////////////////////////////////////////////
# Here are lines to be edited, if desired.
#///////////////////////////////////////////////////////////////////////////////

SVNURL = http://svn.code.sf.net/p/nucnet-tools/code/trunk

NUCNET_TARGET = ../nucnet-tools-code
VENDORDIR = $(NUCNET_TARGET)/vendor
OBJDIR = $(NUCNET_TARGET)/obj

NNT_DIR = $(NUCNET_TARGET)/nnt
USER_DIR = $(NUCNET_TARGET)/user
BUILD_DIR = $(NUCNET_TARGET)/build

GC = h5c++

FC = gfortran

#///////////////////////////////////////////////////////////////////////////////
# End of lines to be edited.
#///////////////////////////////////////////////////////////////////////////////

#===============================================================================
# Svn.
#===============================================================================

SVN_CHECKOUT := $(shell if [ ! -d $(NUCNET_TARGET) ]; then svn co $(SVNURL) $(NUCNET_TARGET); else svn update $(NUCNET_TARGET); fi )

#===============================================================================
# Includes.
#===============================================================================

include $(BUILD_DIR)/Makefile
include $(BUILD_DIR)/Makefile.sparse
include $(USER_DIR)/Makefile.inc

VPATH = $(BUILD_DIR):$(NNT_DIR):$(USER_DIR)

#===============================================================================
# Debugging, if desired.
#===============================================================================

ifdef DEBUG
  CFLAGS += -DDEBUG
  FFLAGS += -DDEBUG
endif

ifdef WN_DEBUG
  CFLAGS += -DWN_DEBUG
endif

#===============================================================================
# Objects.
#===============================================================================

DEPS_OBJ = $(OBJDIR)/ice_utilities.o

$(DEPS_OBJ): $(OBJDIR)/%.o: %.cpp
	$(CC) -c -o $@ $<

ICE_OBJ = $(WN_OBJ)          \
          $(NNT_OBJ)         \
          $(HD5_OBJ)         \
          $(SOLVE_OBJ)       \
          $(USER_OBJ)        \
          $(DECAY_OBJ)       \
          $(ILU_OBJ)         \
          $(MULTI_OBJ)       \
          $(SP_OBJ)          \
          $(DEPS_OBJ)        \

CFLAGS += -DSPARSKIT2 -lgfortran -gstabs
ICE_DEP= sparse
CLIBS+= -lboost_random
FLIBS= -L$(SPARSKITDIR) -lskit
MC = $(FF)

ICE_DEP += $(ICE_OBJ)

#===============================================================================
# Executables.
#===============================================================================

ICE_EXEC = ice

$(ICE_EXEC): $(ICE_DEP)
	$(CC) -c -o $(OBJDIR)/$@.o $@.cpp
	$(CC) $(ICE_OBJ) -o $(BINDIR)/$@ $(OBJDIR)/$@.o $(CLIBS) $(FLIBS)

.PHONY all_ice: $(ICE_EXEC)

#===============================================================================
# Get data.
#===============================================================================

ifndef ICE_DATA_DIR
ICE_DATA_DIR=data
endif

ICE_DATA_URL=http://nucnet-projects.sourceforge.net/data_pub/2018-03-08

ifndef ICE_DATA
ICE_DATA=ice_data.tar.gz
endif

ice_data:
	wget ${ICE_DATA_URL}/${ICE_DATA}
	mkdir -p ${ICE_DATA_DIR}
	tar xz -C ${ICE_DATA_DIR} -f ${ICE_DATA}
	rm ${ICE_DATA}

#===============================================================================
# Clean up. 
#===============================================================================

.PHONY: clean_ice cleanall_ice

clean_ice: clean_lskit
	rm -f $(ICE_OBJ)

cleanall_ice: clean_ice
	rm -f $(BINDIR)/$(ICE_EXEC) $(BINDIR)/$(ICE_EXEC).exe

clean_ice_data:
	rm -fr ${ICE_DATA_DIR}
