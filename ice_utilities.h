////////////////////////////////////////////////////////////////////////////////
//  
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header for utilities for multi-zone chemical evolution code.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef ICE_UTILITIES_H
#define ICE_UTILITIES_H

//##############################################################################
// Includes.
//##############################################################################

#include <algorithm>
#include <functional>
#include <iostream>
#include <fstream>

#include <boost/bind.hpp>
#include <boost/assign.hpp>
#include <boost/heap/fibonacci_heap.hpp>
#include <boost/random/random_device.hpp>
#include <boost/range/numeric.hpp>
#include <boost/program_options.hpp>
#include <boost/tokenizer.hpp>
#include <boost/token_functions.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

#include "nnt/iter.h"
#include "nnt/string_defs.h"

#include "user/hdf5_routines.h"
#include "user/multi_zone_utilities.h"
#include "user/nuclear_decay_utilities.h"

//##############################################################################
// Ice-specific includes.
//##############################################################################

#include "ice_utilities_def.h"
#include "ice_utilities_types.h"
#include "default/star.h"

//##############################################################################
// Prototypes.
//##############################################################################

boost::tuple<std::string,std::string,std::string,double>
model_input(
  int, char **,
  po::options_description&,
  po::options_description&
);

ice_data_t
get_input( int, char ** );

void
insert_new_star(
  Libnucnet *,
  Libnucnet *,
  Libnucnet *,
  double,
  Star&
);

void
update_stars(
  boost::mt19937&,
  ice_data_t&,
  double,
  double
);

void
set_zones( std::vector<nnt::Zone>&, ice_data_t& );

void
add_ejecta( ice_data_t&, double );

boost::tuple<int,int,int>
compute_3d_grid_location(
  boost::mt19937&,
  Libnucnet *
);
  
boost::tuple<double,double,double>
compute_3d_subgrid_location(
  boost::mt19937&
);

void
set_links(
  user::zone_link_graph_t& g,
  std::vector<nnt::Zone>& zones,
  ice_data_t& ice_data
);

void
create_multi_d_zones(
  boost::tuple<int,int,int>&,
  ice_data_t&
);
  
void
update_common_zone_properties(
  std::vector<nnt::Zone>&,
  ice_data_t&
);

boost::tuple<int,int,int>
compute_3d_grid_location(
  boost::mt19937&,
  Libnucnet *
);
  
boost::tuple<double,double,double>
compute_3d_subgrid_location(
  boost::mt19937&
);

void
set_links(
  user::zone_link_graph_t& g,
  std::vector<nnt::Zone>& zones,
  ice_data_t& ice_data
);

void
create_multi_d_zones(
  boost::tuple<int,int,int>&,
  ice_data_t&
);
  
void
update_common_zone_properties(
  std::vector<nnt::Zone>&,
  ice_data_t&
);

void
update_history_zones(
  ice_data_t&
);

std::map<std::string,double>
compute_total_masses( ice_data_t& );

double
compute_total_active_star_mass( ice_data_t& );

void
output_imf_deviate( ice_data_t& );

void
output_star_pop( ice_data_t& );

void
output_mass_data( ice_data_t& );

void
clean_up( ice_data_t& );

#endif  // ICE_UTILITIES_H
